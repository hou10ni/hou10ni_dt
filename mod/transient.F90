!> @file transient.F90
!> @todo : add a resume
module transient
  use precision
  use constant, only : large_char_len
  implicit none
  
  character (len=large_char_len) :: file_src_time


  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: a_flu(:)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: a_sol(:)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: a_flusol(:)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: u_old(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: u_new(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: u_inter(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: src_val(:) 

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: a_cla_flu(:)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: a_cla_sol(:)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: p_old(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: p_new(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: p_inter(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: vec_source(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: q_new(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: q_old(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: zz(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: q_inter(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: qu_new(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: qu_old(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: zzu(:, :)

  !> @todo : document this module variable
  real (kind=dp), allocatable, save :: qu_inter(:, :)


  !> @todo : document this module variable
  integer, allocatable, save :: idx_mat_sol(:)

  !> @todo : document this module variable
  integer, allocatable, save :: idx_vec_sol(:, :)

  !> @todo : document this module variable
  integer, allocatable, save :: src_idx(:, :)

  !> @todo : document this module variable
  integer, allocatable, save :: idx_mat_flu(:)

  !> @todo : document this module variable
  integer, allocatable, save :: idx_vec_flu(:, :)

  !> @todo : document this module variable
  integer, allocatable, save :: idx_mat_cla_flu(:)

  !> @todo : document this module variable
  integer, allocatable, save :: idx_vec_cla_flu(:, :)

  !> @todo : document this module variable
  integer, allocatable, save :: idx_mat_cla_sol(:)

  !> @todo : document this module variable
  integer, allocatable, save :: idx_vec_cla_sol(:, :)

  !> @todo : document this module variable
  integer, save :: src_nbddl

  !> @todo : document this module variable
  integer, save :: n_simu

  !> @todo : document this module variable
  integer, save :: type_src_time

  !> @todo : document this module variable
  integer, save :: size_source

  !> @todo : document this module variable
  integer, save :: n_sismo

  !> @todo : document this module variable
  integer, save :: n_beg

  !> @todo : document this module variable
  integer, save :: n_end

  !> @todo : document this module variable
  integer, save :: nslow
  
  !> @todo : document this module variable
  real (kind=dp), save :: t_simu

  !> @todo : document this module variable
  real (kind=dp), save :: dt

  !> @todo : document this module variable
  real (kind=dp), save :: fpeak

  !> @todo : document this module variable
  real (kind=dp), save :: dt_snap

  !> @todo : document this module variable
  real (kind=dp), save :: dt_sismo

  !> @todo : document this module variable
  real (kind=dp), save :: alpha0

  !> @todo : document this module variable
  real (kind=dp), save :: dt_source

  !> @todo : document this module variable
  real (kind=dp), save :: dt_coarse

  !> @todo : document this module variable
  real (kind=dp), save :: dt_fine

  !> @todo : document this module variable
  real (kind=dp), save :: current_time

  !> @todo : document this type
  type vector_real
     real(kind=dp), allocatable :: value(:)
  end type vector_real

  !> @todo : document this type
  type :: array_real
    real (kind=dp), allocatable :: coeff(:, :)
  end type

  !> @todo : document this type
  type :: array_int
    integer, allocatable :: value(:, :)
  end type

  !> @todo : document this module variable
  type (array_real), allocatable :: mass_mat(:)

  !> @todo : document this module variable
  type (vector_real), allocatable :: comm_ghost_flu(:)

  !> @todo : document this module variable
  type (vector_real), allocatable :: comm_ghost_rec_flu(:)

  !> @todo : document this module variable
  type (array_int), allocatable :: comm_ghost_send2rcv_flu(:)

  !> @todo : document this module variable
  type (vector_real), allocatable :: comm_ghost_sol(:)

  !> @todo : document this module variable
  type (vector_real), allocatable :: comm_ghost_rec_sol(:)

  !> @todo : document this module variable
  type (array_int), allocatable :: comm_ghost_send2rcv_sol(:)

  !> @todo : document this module variable
  integer, allocatable :: idx_ghost_flu(:)

  !> @todo : document this module variable
  integer, allocatable :: idx_ghost_sol(:)

  !> @todo : document this module variable
  integer, allocatable :: tag_recv_sol(:)

  !> @todo : document this module variable
  integer, allocatable :: tag_send_sol(:)

  !> @todo : document this module variable
  integer, allocatable :: req_recv_sol(:)

  !> @todo : document this module variable
  integer, allocatable :: req_send_sol(:)

  !> @todo : document this module variable
  integer, allocatable :: tag_recv_flu(:)

  !> @todo : document this module variable
  integer, allocatable :: tag_send_flu(:)

  !> @todo : document this module variable
  integer, allocatable :: req_recv_flu(:)

  !> @todo : document this module variable
  integer, allocatable :: req_send_flu(:)

  !> @todo : document this module variable
  integer :: nsol_total

  !> @todo : document this module variable
  integer :: nflu_total
end module
