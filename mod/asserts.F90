!> @file asserts.F90
! 
!> @brief this module contains the assert subroutine for various type
!> @note : a bunch of code could be factorize using preprocessor like Fypp
!> @note : prec argument could be optional
module asserts

  use precision
  implicit none
  public :: assert_equals

  interface assert_equals

    module procedure :: assert_equals_d
    module procedure :: assert_equals_c
    module procedure :: assert_equals_q
    module procedure :: assert_equals_vec
    module procedure :: assert_equals_mat
    module procedure :: assert_equals_dq_vec
    module procedure :: assert_equals_integer_vec
    module procedure :: assert_equals_integer
    module procedure :: assert_equals_string
    module procedure :: assert_equals_logical

  end interface


contains

! assert subroutine to test equality between integer vectors
  subroutine assert_equals_integer_vec(x, y)
    integer, intent (in) :: x(:), y(:)

    if (any((x/=y))) then
      print *, abs(x-y)
      stop -1
    end if

  end subroutine

! assert subroutine to test equality between integer
  subroutine assert_equals_integer(x, y)
    integer, intent (in) :: x, y

    if (x/=y) then
      print *, abs(x-y)
      stop -1
    end if

  end subroutine

! assert subroutine to test equality between strings
  subroutine assert_equals_string(x, y)
    character (*), intent (in) :: x, y

    if (x/=y) then
      print *, x // '/=' // y
      stop -1
    end if

  end subroutine

! assert subroutine to test equality between logical
  subroutine assert_equals_logical(x, y)
    logical, intent (in) :: x, y

    if (x .neqv. y) then
      print *, x, '.neqv.', y
      stop -1
    end if

  end subroutine

! assert subroutine to test equality between reals
  subroutine assert_equals_d(x, y, prec)
    real (dp), intent (in) :: x, y
    real (dp), optional :: prec
    real (dp) :: eps = 1.d-12

    if (present(prec)) then
      eps = prec
    end if
    if (abs(x-y)>eps) then
      print *, abs(x-y)
      stop -1
    end if

  end subroutine

! assert subroutine to test equality quadruple precision reals
  subroutine assert_equals_q(x, y, prec)
    real (dq), intent (in) :: x, y
    real (dq), intent (in) :: prec

    if (abs(x-y)>prec) then
      print *, abs(x-y)
      stop -1
    end if

  end subroutine

! assert subroutine to test equality between complex
  subroutine assert_equals_c(x, y, prec)
    complex (dp), intent (in) :: x, y
    real (dp), intent (in) :: prec

    if (abs(x-y)>prec) then
      print *, abs(x-y)
      stop -1
    end if

  end subroutine

  subroutine assert_equals_vec(x, y, prec)
    real (dp), intent (in) :: x(:), y(:), prec

    if (sum(abs(x-y))>prec) then
      print *, abs(x-y)
      stop -1
    end if

  end subroutine

  subroutine assert_equals_mat(x, y, prec)
    real(dp), intent (in) :: x(:,:), y(:,:)
    real(dp), optional :: prec
    real(dp) :: eps
    if (present(prec)) then
        eps = prec
    else
        eps = 10.d0 * epsilon(1.d0)
    end if

    if (sum(abs(x-y))>eps) then
      print *, abs(x-y)
      stop -1
    end if

  end subroutine


  subroutine assert_equals_dq_vec(x, y, prec)
    real (dq), intent (in) :: x(:), y(:), prec

    if (sum(abs(x-y))>prec) then
      print *, abs(x-y)
      stop -1
    end if

  end subroutine


end module
