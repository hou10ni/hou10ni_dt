MODULE curved
  USE PRECISION
  USE matrix
  REAL(kind=dp),DIMENSION(:,:,:),ALLOCATABLE,SAVE ::Phi1D_Quad_inter
  REAL(kind=dp),DIMENSION(:,:,:,:),ALLOCATABLE,SAVE ::Gradphi2D1D_quad_Fk
  INTEGER,DIMENSION(:,:),ALLOCATABLE,SAVE :: phiedge_curved
  TYPE(value_ref),SAVE :: value_basis_curved,value_basis_curved_1D
  TYPE(base),SAVE :: basis_curved,basis_curved_1D

END MODULE curved
