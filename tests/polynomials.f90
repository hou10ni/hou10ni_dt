module polynomials

    use precision
    use asserts
    use polynom
    implicit none
    real(dq) :: res, check
    real(dq), allocatable :: check_vec(:)
    integer ::  i
    type(polynome_p) :: p1, p2, p3

contains

    subroutine test_polynom_p_eval_1D
       call construct_poly_p(1, 8, p1)

       p1 % coeff = [ (i*1._dq, i=1,9) ]

       res = valpoly_p(p1, [2.89_dq])

       check = 63030.42526178_dq
       call assert_equals(res, check, 1.e-10_dq)


    end subroutine test_polynom_p_eval_1D


    subroutine test_polynom_p_eval_2D
       call construct_poly_p(2, 3, p1)

       p1 % coeff = [12.0_dq,  1.3_dq, -2.4_dq, 3.5_dq, 7.2_dq, 8.9_dq,  -12.28_dq, 0.1_dq, -0.2_dq,  2.3_dq ]

       res = valpoly_p(p1, [.61_dq, -7.3737_dq])

       check = -448.49533817325175d0
       call assert_equals(res, check, 1.e-12_dq)

    end subroutine test_polynom_p_eval_2D


    subroutine test_polynom_p_eval_3D
       call construct_poly_p(3, 3, p1)

       p1 % coeff = [6, -10,  -3,  8,  4,  -1,  7,  6,  -6,  9,  -1,  2,  -3,  -4,  5,  6,  4,  5,  -6,  2]*1._dq

       res = valpoly_p( p1, [4._dq*atan(1._dq), -exp(1._dq), -1._dq ] )

       ! build_p3([6,-10,-3,8,4,-1,7,6,-6,9,-1,2,-3,-4,5,6,4,5,-6,2])(x=>π,y=>-exp(1),z=>-1) in scripts/polynomials.jl

       check =-151.54675585913833_dq

       call assert_equals(res, check, 1.e-12_dq)

    end subroutine test_polynom_p_eval_3D


    subroutine test_polynom_p_mult_1D
       call construct_poly_p(1, 8, p1)
       call construct_poly_p(1, 2, p2)

       p1 % coeff = [ (i*1._dq, i=1,9) ]
       p2 % coeff = [ (i*1._dq, i=3,1,-1) ]

       p3 = p1 * p2
       check_vec = [3, 8, 14, 20, 26, 32, 38, 44, 50, 26, 9] * 1._dq

       call assert_equals(p3 % degree, 10)
       call assert_equals(p3 % coeff, check_vec, 1.e-10_dq)

    end subroutine test_polynom_p_mult_1D

end module polynomials
