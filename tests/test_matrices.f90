program test_matrices
    use iso_fortran_env
    use asserts
    use mesh ,  only : basis, matrices
    use matrix , only : construct_matrix_volume
    use basis_functions, only : construct_basis, construct_basis_diff, & 
                                construct_basis_gamma, construct_basis_gamma_neigh 

    use polynom , only : construct_coe
    implicit none
    integer :: deg, dim
    character(len=256) :: arg

    call get_command_argument( 1, arg )
    read (arg, '(i1)') deg

    call get_command_argument( 2, arg )
    read (arg, '(i1)') dim
     
    allocate(basis(0:deg))
    call construct_coe( dim, deg+1 )
    call construct_coe( dim-1, deg+1 )
    allocate(matrices%matvolume(0:deg))

    call test_matrice( deg , dim )

    deallocate(basis)
    deallocate(matrices%matvolume)
 
contains

   subroutine test_matrice(deg, dim)
     integer, intent(in) :: deg, dim
     call construct_basis(1, dim, deg, basis(deg))
     call construct_basis_diff(basis(deg), 1)

     allocate(matrices%matvolume(deg)%dkphiIdlphiJ(2))

     call construct_matrix_volume(basis(deg), matrices%matvolume(deg)%dkphiIdlphiJ(1), 0, 0)  
     call construct_matrix_volume(basis(deg), matrices%matvolume(deg)%dkphiIdlphiJ(2), 0, 1)  

     deallocate( matrices%matvolume(deg)%dkphiIdlphiJ )


   end subroutine test_matrice

end program test_matrices


