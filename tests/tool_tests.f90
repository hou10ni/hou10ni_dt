module tool_tests
    use constant
    use asserts
    use tools
    implicit none

contains

     function old_justify(n) result(nam)
        character(len=30) :: nam
        integer , intent(in ):: n
        write(nam,*) n
        if (n.le.9) then
            nam="000"//trim(adjustl(nam))
        elseif (n.le.99) then
            nam="00"//trim(adjustl(nam))
        elseif (n.le.999) then
            nam="0"//trim(adjustl(nam))
        else
            nam = trim(adjustl(nam))
        end if
     end function
    
    subroutine test_justify_number
      character(len=100) :: str_1, str_2
      integer :: n(5) = [1, 22, 333, 4444, 5555]
      integer :: i
      do i =1,5
         str_1 = old_justify( n(i) )
         str_2 = justify_number( n(i), 4 )
         call assert_equals( str_1, str_2 )
      end do

    end subroutine test_justify_number



    subroutine test_normal_2D

     real(kind=8), allocatable :: v(:,:), norm_check(:,:)

     v = reshape([-45.41396543123733d0, -38.692125795007314d0, 84.106091226244644d0, &
                   113.83519151769178d0, -81.412454986776083d0, -32.422736530915699d0], [3,2])
     norm_check = reshape([ 0.92881424506796162d0, -0.90318629199743095d0, -0.35969645171029307d0,& 
                            0.37054567621122325d0, 0.42924878793996762d0, -0.93306937717783067d0], [3,2])
     call assert_equals(norm_check, compute_normals(v), 1.d-9)
    end subroutine


    subroutine test_normal_3D

      real(kind=dp), allocatable :: v(:,:), norm_check(:,:), area_check(:)
      real(kind=dp) :: norm(4,3), area(4)

      v = reshape(&
      [ 0.35289168971014639d0, 0.81347474350250515d0, 0.4067210565550976d0, 0.46058305379235875d0, 0.05382936684495121d0, &
      -0.38950917399328211d0, -0.3270707350456783d0, 0.46926954256927433d0, 0.062438438947603814d0, 0.85877871656255644d0, &
      0.20714795446247725d0, -0.050849780683047463d0, 0.052614000075487888d0, -0.25799773514552471d0, -0.15453395438698936d0],&
          [5, 3])
      norm_check = reshape(&
      [0.47151087148874671d0, -0.012827558246872189d0, -0.33539009816434956d0, -0.30390565527142183d0, &
      0.12746603901526266d0,  0.12238459705107341d0,   0.18716691671954372d0, -0.6471611580967197d0, & 
      0.87259951121100343d0, -0.99239985094419958d0,   0.92329953283807498d0, -0.69915934388732892d0 ], [4, 3])

      area_check = [0.44943630741513851d0, 0.51870773177902585d0, 0.3509401803716235d0, 0.28811076161990129d0 ]

      call compute_normals_3D(v, norm, area)
      call assert_equals(norm_check, norm, 1.d-9)
      call assert_equals(area_check, area, 1.d-9)

    end subroutine test_normal_3D
    
   subroutine test_cross
        
     real(kind=dp), allocatable :: u(:), v(:), w_check(:)
    
     u = [0.7300188573384594d0, 0.966846822040069d0,  0.9771593534883951d0 ]
     v = [0.8213688640597934d0, 0.0059408164925640605d0,  0.2805624262735318d0 ]
     w_check = [0.2654557658233483d0, 0.5977924063398558d0, -0.7898009678713136d0 ]

     call assert_equals( cross(u,v), w_check, 1.d-9)

   end subroutine test_cross


   subroutine test_mixed_product

     real(kind=dp), allocatable :: v(:,:)
     real(kind=dp), parameter :: dfvec_check = 0.13370742747778874d0

     v = reshape( [&
     0.35289168971014639d0, 0.81347474350250515d0, 0.4067210565550976d0, 0.46058305379235875d0, 0.05382936684495121d0, &
     -0.38950917399328211d0, -0.3270707350456783d0, 0.46926954256927433d0, 0.062438438947603814d0, 0.85877871656255644d0, &
     0.20714795446247725d0, -0.050849780683047463d0, 0.052614000075487888d0, -0.25799773514552471d0, -0.15453395438698936d0]&
     , [5,3] )

     call assert_equals( mixed_product( v(1,:), v(2, :), v(3,:) ), dfvec_check, 1.d-9 )

   end subroutine

   subroutine test_jf_inv_3D

     real(kind=dp), allocatable :: v(:,:), jf_inv_check(:,:)
     real(kind=dp), parameter :: dfvec_check = 0.13370742747778874d0
     real(kind=dp) :: jf_inv(3,3), dfvec

     v = reshape( [&
     0.35289168971014639d0, 0.81347474350250515d0, 0.4067210565550976d0, 0.46058305379235875d0, 0.05382936684495121d0, &
     -0.38950917399328211d0, -0.3270707350456783d0, 0.46926954256927433d0, 0.062438438947603814d0, 0.85877871656255644d0, &
     0.20714795446247725d0, -0.050849780683047463d0, 0.052614000075487888d0, -0.25799773514552471d0, -0.15453395438698936d0]&
     , [5,3] )
     jf_inv_check = reshape( [&
     0.049763530478542199d0, -0.47478167771642932d0, 3.8499392697284605d0, &
     0.88029411503116217d0, -0.49125462027207639d0, -2.4233725134309076d0, &
     0.65485135308126075d0, 1.3944931681602857d0, 1.5065380799021872d0], [3,3])

     call compute_jacobians_3D( v, dfvec, jf_inv)
     call assert_equals( dfvec, dfvec_check, 1.d-9 )
     call assert_equals( jf_inv, jf_inv_check, 1.d-9 )

   end subroutine

   subroutine test_jf_inv_2D

     real(kind=dp), allocatable :: v(:,:), jf_inv_check(:,:)
     real(kind=dp), parameter :: dfvec_check = 185.58931997213858d0
     real(kind=dp) :: jf_inv(2,2), dfvec

     v = reshape([ -5.9730810382468462d0, -10.774865339802659d0, 16.747946378049505d0, & 
                   12.662173085598909d0, -8.2296070906756995d0, -4.432565994923209d0], [3,2] )
     jf_inv_check = reshape( [0.04434310709210617d0, -0.058057572178292507d0,&
                              0.02388373423421479d0, 0.09024197287087303d0], [2, 2])

     call compute_jacobians_2D( v, dfvec, jf_inv)
     call assert_equals( dfvec, dfvec_check, 1.d-9 )
     call assert_equals( jf_inv, jf_inv_check, 1.d-9 )

   end subroutine
   

   subroutine test_compute_v_2d

     real(kind=8) :: v(3,2) , v_check(3,2), coor(8, 2)
     integer , parameter ::  node(3) = [5, 3, 2]

     call random_number( coor ) 

     v = compute_v_2d( coor, node )

     v_check(1,1) = coor(node(3), 1) - coor(node(2), 1)
     v_check(1,2) = coor(node(3), 2) - coor(node(2), 2)
     v_check(2,1) = coor(node(1), 1) - coor(node(3), 1)
     v_check(2,2) = coor(node(1), 2) - coor(node(3), 2)
     v_check(3,1) = coor(node(2), 1) - coor(node(1), 1)
     v_check(3,2) = coor(node(2), 2) - coor(node(1), 2)

     call assert_equals( v, v_check )

   end subroutine

   subroutine test_compute_v_3d

     real(kind=8) :: v(5,3) , v_check(5,3), coor(8, 3)
     integer , parameter ::  node(4) = [5, 3, 2, 7 ]

     call random_number( coor ) 

     v = compute_v_3d( coor, node )

     v_check(1,:)=Coor(Node(2),:)-Coor(Node(1),:)
     v_check(2,:)=Coor(Node(3),:)-Coor(Node(1),:)
     v_check(3,:)=Coor(Node(4),:)-Coor(Node(1),:)
     v_check(4,:)=Coor(Node(3),:)-Coor(Node(2),:)
     v_check(5,:)=Coor(Node(4),:)-Coor(Node(2),:)
     
     call assert_equals( v, v_check )


   end subroutine

end module tool_tests
