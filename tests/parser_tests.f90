module parser_tests
  use parser
  use asserts
  use iso_fortran_env
  implicit none
  character(len=82) :: param
  type(param_parser) :: parse_file

contains

  subroutine test_parse_string()
    character(len=182) :: value, default_value
    default_value = "hihi"
    parse_file = param_parser("dummy.par")

    ! test for present value
    value = parse_file % get_string( "dummy", default_value )
    call assert_equals( trim( value ) , "hehe") 

    ! test for absent value
    value = parse_file % get_string( "dummy2", default_value )
    call assert_equals( trim(value) , "hihi")

  end subroutine test_parse_string

  subroutine test_parse_real()
    real(kind=8) :: value, default_value = 0
    parse_file = param_parser("dummy.par")

    ! test for present value of dx
    value = parse_file % get_real( "omega", default_value )
    call assert_equals( value , 0.5d0 )

    ! test for absent value
    default_value = -129.
    value = parse_file % get_real( "jivot", default_value )
    call assert_equals( value , default_value)


  end subroutine test_parse_real

  subroutine test_parse_integer()
    integer :: value, default_value
    default_value = 0
    parse_file = param_parser("dummy.par")

    ! test for present value
    value = parse_file % get_integer( "dum_int", default_value )
    call assert_equals( value , -2 )

    ! test for absent value
    value = parse_file % get_integer( "nu", default_value )
    call assert_equals( value , default_value )

  end subroutine test_parse_integer


  subroutine test_parse_logical()
    logical :: value, default_value
    parse_file = param_parser("dummy.par")
    default_value = .false.

    ! test for present value
    value = parse_file % get_logical( "symmetry", default_value )
    call  assert_equals( value , .true. )

    ! test for absent value
    value = parse_file % get_logical( "helmut", default_value )
    call assert_equals( value , default_value )

  end subroutine test_parse_logical

  
end module parser_tests
