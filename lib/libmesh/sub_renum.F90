subroutine sub_renum
  use mesh
  implicit none
  integer :: i, j, test
  ! @todo : check if save is mandatory
  integer, allocatable, save :: flusol(:, :)
  integer, allocatable, save :: triinter(:,:)

  nflu = 0
  nsol = 0
  nflusol = 0
  nsolflu = 0
  nporo = 0
  nem = 0
  allocate (flusol(2,ntri))
  flusol = 0

  do i = 1, ntri
    if (acouela(ref_media(i))==1) then
      test = 0
      do j = 1, nfacesperelem
        if (neigh(i,j)>0) then
          if (acouela(ref_media(neigh(i,j)))/=1) then
            test = 1
          end if
        end if
      end do
      if (test==0) then
        nflu = nflu + 1
        flusol(1, i) = 1
        flusol(2, i) = nflu
      else
        nflusol = nflusol + 1
        flusol(1, i) = 2
        flusol(2, i) = nflusol
      end if
    else if (acouela(ref_media(i))==2) then
      test = 0
      do j = 1, nfacesperelem
        if (neigh(i,j)>0) then
          if (acouela(ref_media(neigh(i,j)))==1) then
            test = 1
          end if
        end if
      end do
      if (test==0) then
        nsol = nsol + 1
        flusol(1, i) = 4
        flusol(2, i) = nsol
      else
        nsolflu = nsolflu + 1
        flusol(1, i) = 3
        flusol(2, i) = nsolflu
      end if
    else if (acouela(ref_media(i))==3) then
      nporo = nporo + 1
      test = 0
      do j = 1, nfacesperelem
        if (neigh(i,j)>0) then
          if (acouela(ref_media(neigh(i,j)))==1) then
            test = 1
          end if
        end if
      end do
      if (test==0) then
        ! Nsol=Nsol+1
        flusol(1, i) = 4
        flusol(2, i) = nporo
      else
        nsolflu = nsolflu + 1
        flusol(1, i) = 3
        flusol(2, i) = nsolflu
      end if
    else if (acouela(ref_media(i))==4) then
      nem = nem + 1
      test = 0
      do j = 1, nfacesperelem
        if (neigh(i,j)>0) then
          if (acouela(ref_media(neigh(i,j)))==1) then
            test = 1
          end if
        end if
      end do
      if (test==0) then
        ! Nsol=Nsol+1
        flusol(1, i) = 4
        flusol(2, i) = nem
      else
        nsolflu = nsolflu + 1
        flusol(1, i) = 3
        flusol(2, i) = nsolflu
      end if
    else if (acouela(ref_media(i))==5) then
      nsem = nsem + 1
      test = 0
      do j = 1, nfacesperelem
        if (neigh(i,j)>0) then
          if (acouela(ref_media(neigh(i,j)))==1) then
            test = 1
          end if
        end if
      end do
      if (test==0) then
        ! Nsol=Nsol+1
        flusol(1, i) = 4
        flusol(2, i) = nsem
      else
        nsolflu = nsolflu + 1
        flusol(1, i) = 3
        flusol(2, i) = nsolflu
      end if
    end if
  end do
  if (nflu+nflusol+nsolflu+nsol+nporo+nem+nsem/=ntri) then
    write (6, *) 'error nb elem in renum', nflu + nflusol + nsolflu + nsol + &
      nporo + nem + nsem, ntri
    stop
  end if
  allocate (triinter(ntri,nfacesperelem))
  do i = 1, ntri
    select case (flusol(1,i))
    case (1)
      triinter(flusol(2,i), :) = tri(i, :)
    case (2)
      triinter(nflu+flusol(2,i), :) = tri(i, :)
    case (3)
      triinter(nflu+nflusol+flusol(2,i), :) = tri(i, :)
    case (4)
      triinter(nflu+nflusol+nsolflu+flusol(2,i), :) = tri(i, :)
    end select
  end do
  tri = triinter
  triinter = 0
  do i = 1, ntri
    select case (flusol(1,i))
    case (1)
      triinter(flusol(2,i), :) = neigh(i, :)
    case (2)
      triinter(nflu+flusol(2,i), :) = neigh(i, :)
    case (3)
      triinter(nflu+nflusol+flusol(2,i), :) = neigh(i, :)
    case (4)
      triinter(nflu+nflusol+nsolflu+flusol(2,i), :) = neigh(i, :)
    end select
  end do
  neigh = triinter
  triinter = 0
  do i = 1, ntri
    select case (flusol(1,i))
    case (1)
      triinter(flusol(2,i), 1) = ref_media(i)
    case (2)
      triinter(nflu+flusol(2,i), 1) = ref_media(i)
    case (3)
      triinter(nflu+nflusol+flusol(2,i), 1) = ref_media(i)
    case (4)
      triinter(nflu+nflusol+nsolflu+flusol(2,i), 1) = ref_media(i)
    end select
  end do
  ref_media = triinter(:, 1)
  triinter = 0
  do i = 1, ntri
    select case (flusol(1,i))
    case (1)
      triinter(flusol(2,i), 1) = degree_tri(i)
    case (2)
      triinter(nflu+flusol(2,i), 1) = degree_tri(i)
    case (3)
      triinter(nflu+nflusol+flusol(2,i), 1) = degree_tri(i)
    case (4)
      triinter(nflu+nflusol+nsolflu+flusol(2,i), 1) = degree_tri(i)
    end select
  end do
  degree_tri = triinter(:, 1)
  deallocate (triinter)
  do i = 1, ntri
    do j = 1, nfacesperelem
      if (neigh(i,j)>0) then
        select case (flusol(1,neigh(i,j)))
        case (1)
          neigh(i, j) = flusol(2, neigh(i,j))
        case (2)
          neigh(i, j) = nflu + flusol(2, neigh(i,j))
        case (3)
          neigh(i, j) = nflu + nflusol + flusol(2, neigh(i,j))
        case (4)
          neigh(i, j) = nflu + nflusol + nsolflu + flusol(2, neigh(i,j))
        end select
      end if
    end do
  end do

  deallocate (flusol)
end subroutine
