SUBROUTINE sub_refine
  USE mesh
  USE data
  IMPLICIT NONE
  INTEGER :: I,II,J,K,Node(6),Test, Local_Edge
  INTEGER :: Iraff,Inoraff,Iboundraff,Iboundnoraff,Ineigh, Nedge_temp
  INTEGER,ALLOCATABLE :: ref_mediainter(:)
  INTEGER,DIMENSION(:,:),ALLOCATABLE,SAVE :: TriInter,NeighInter, Edge_ptInter, Edge_temp
  INTEGER,DIMENSION(:), ALLOCATABLE,SAVE :: degree_triInter, bc_edgeInter, bc_edge_local
  REAL*8,DIMENSION (:,:),ALLOCATABLE,SAVE :: CoorInter
  REAL*8 :: V12(2),V13(2),V23(2),x,y,h1, Length_edge_tmp
  !!h=0.D0
  !!hraff=0.D0
  !!Nraff=0
  ALLOCATE(TriInter(4*Ntri,3)) !! the number of triangle will be multiplied by 4
  ALLOCATE(degree_triInter(4*Ntri)) !! the degree of the triangles
  ALLOCATE(ref_mediaInter(4*Ntri)) !! the number of triangle will be multiplied by 4
  ALLOCATE(NeighInter(4*Ntri,6)) !! the number of neighs also. We need three
  !! more neigh to simplify the process (see below)
  ALLOCATE(CoorInter(NPoints+3*NTri,2)) !! The division of each triangles
  !! creates three additionnal points (the middle of each edge)
  !! For the edges : each original edge creates two edges and each triangle
  !! creates three additional edges : Nedges_new=2Nedge+3Ntri (only for DG!!)
  ALLOCATE(Edge_ptInter(2*Nedge+3*NTri,2))
  ALLOCATE(Edge_temp(9,2)) !!the local edges of one old triangle
  ALLOCATE(bc_edgeInter(2*Nedge+3*NTri)) !! the boundary conditions of the edges, the final size will be smaller in HDG
  ALLOCATE(bc_edge_local(9))
  TriInter=0
  TriInter(1:Ntri,:)=Tri !! The Ntri first elements are the original ones
  NeighInter=0
  NeighInter(1:Ntri,1:3)=Neigh !! We keep the Ntri first neighbours
  CoorInter=0.D0
  CoorInter(1:NPoints,:)=Coor !! The Npoints first points are the original ones

  Nedge_temp =0 !! temporary counter for the edges
  bc_edgeInter=0 !!if the edge is inside the mesh, the bc is zero, otherwise it's 4 or 3 (abc)

  degree_triInter(1:Ntri)=degree_tri

  Length_edge_max=0.
  DO I=1,Ntri     !! For all original triangles
     Node(1:3) = Tri(I,:)      !! We get the three nodes of the triangle
     !! Node(4) will be the middle of edge 1
     !! Node(5) will be the middle of edge 2
     !! Node(6) will be the middle of edge 3
     !! In the new meshes, triangle I will be the center (the one which is
     !! define Node(4), Node(5) and Node(6))
     !! Triangle Ntri+1 will be the one defined by Node(1) and the middles of
     !! edges 3 and 2 : Node(1), Node(6), Node(5)
     !! Triangle Ntri+2 will be the one defined by Node(2) and the middles of
     !! edges 1 and 3: Node(2), Node(4), Node(6)
     !! Triangle Ntri+3 will be the one defined by Node(3) and the middles of
     !! edges 2 and 1: Node(3), Node(5), Node(4)

     V12(1)=Coor(Node(2),1)-Coor(Node(1),1)!! we compute the vectors of the
     !! three edges
     V12(2)=Coor(Node(2),2)-Coor(Node(1),2)
     V13(1)=Coor(Node(3),1)-Coor(Node(1),1)
     V13(2)=Coor(Node(3),2)-Coor(Node(1),2)
     V23(1)=Coor(Node(3),1)-Coor(Node(2),1)
     V23(2)=Coor(Node(3),2)-Coor(Node(2),2)

     bc_edge_local=0

     !        Edge 1 (23)
     x=Coor(Node(2),1)+1/2.D0*V23(1)
     y=Coor(Node(2),2)+1/2.D0*V23(2)
     !! x and y are the coordinate of the middle of edge 1
     Ineigh=Neigh(I,1) ! we get the original neighbor of triangle I
     IF (Ineigh.NE.-1) THEN ! if this triangle exists
        IF(I.LT.Ineigh) THEN !! If I is smaller than Ineigh, this means that we
           !! did not consider Ineigh yet and we did not treat the edge between
           !! the two triangles.

           Npoints=Npoints+1 ! we add a new point : the middle of the edge
           CoorInter(Npoints,1)=x
           CoorInter(Npoints,2)=y
           Node(4)=Npoints
           DO J=1,3!! we run over all the edges of Ineigh to find which one is
              !! between I and Ineigh
              IF (Neigh(INeigh,J).EQ.I) THEN
                 NeighInter(INeigh,J)=Ntri+3  !! The two neighbourgh of Ineigh
                 !! through edge J are Ntri+3 and Ntri+2
                 NeighInter(INeigh,J+3)=Ntri+2
                 EXIT
              END IF
           END DO
        ELSE !! If we already handle this edge
           NeighInter(Ntri+2,1)=NeighInter(I,1) ! the first neighbour of Ntri+2
           NeighInter(Ntri+3,1)=NeighInter(I,4) ! the first neighbour of Ntri+3
           DO J=1,3!! we run over all the edges of Ineigh to find which one is
              !! between I and Ineigh
              IF (Neigh(INeigh,J).EQ.I) THEN
                 NeighInter(NeighInter(I,1),J)=Ntri+2 !! the Jth neighbor of NeighInter(I,1)
                 NeighInter(NeighInter(I,4),J)=Ntri+3!! the Jth neighbor of NeighInter(I,4)
                 SELECT CASE(J)
                 CASE(1)
                    Node(4)=TriInter(NeighInter(Ntri+2,1),2) !! since we
                    !! already handle the edge, its middle is already
                    !! constructed. It should be the second point of the first
                    !! neighbor of Ntri+2
                    IF (Node(4).NE.TriInter(NeighInter(Ntri+3,1),3)) THEN !!It
                       !!should also be the third point of the first
                    !! neighbor of Ntri+3
                       WRITE(6,*) 'error1'
                       STOP
                    END IF
                 CASE(2)
                    Node(4)=TriInter(NeighInter(Ntri+2,1),3) !! since we
                    !! already handle the edge, its middle is already
                    !! constructed. It should be the third point of the first
                    !! neighbor of Ntri+2
                    IF (Node(4).NE.TriInter(NeighInter(Ntri+3,1),1)) THEN !!It
                       !!should also be the first point of the first
                    !! neighbor of Ntri+3
                       WRITE(6,*) 'error4'
                       STOP
                    END IF
                 CASE(3)
                    Node(4)=TriInter(NeighInter(Ntri+2,1),1)!! since we
                    !! already handle the edge, its middle is already
                    !! constructed. It should be the first point of the first
                    !! neighbor of Ntri+2
                    IF (Node(4).NE.TriInter(NeighInter(Ntri+3,1),2)) THEN!!It
                       !!should also be the second point of the first
                    !! neighbor of Ntri+3
                       WRITE(6,*) 'error7'
                       STOP
                    END IF
                 END SELECT
                 EXIT
              END IF
           END DO
        END IF
     ELSE !! If there is no neighbor, we are on the boundary. Obviously, we
        !! never handled this edge
        Npoints=Npoints+1 !! we define a new point which is the center of the edge
        CoorInter(Npoints,1)=x
        CoorInter(Npoints,2)=y
        Node(4)=Npoints
        NeighInter(Ntri+2,1)=-1 !there is no neighbor
        NeighInter(Ntri+3,1)=-1 !there is no neighbor

        !!we find the global number of the edge we are considering
        !!we change the new value of bc_edge
        DO II=1,Nedge
        ! !        write(6,*) "loop",II, Nedge_temp
          IF(((Node(2).eq.Edge_pt(II,1)).and.(Node(3).eq.Edge_pt(II,2))).or.&
               & ((Node(2).eq.Edge_pt(II,2)).and.(Node(3).eq.Edge_pt(II,1))))THEN
          !           !!!this is the global number of the edge
                     exit
          END IF
        END DO
        bc_edge_local(4)=bc_edge(II)
        bc_edge_local(5)=bc_edge(II)
     END IF
     !        Edge 2 (31)
     x=Coor(Node(1),1)+1/2.D0*V13(1)
     y=Coor(Node(1),2)+1/2.D0*V13(2)
     !! x and y are the coordinate of the middle of edge 2
     Ineigh=Neigh(I,2)! we get the original neighbor of triangle I
     IF (Ineigh.NE.-1) THEN! if this triangle exists
        IF(I.LT.Ineigh) THEN!! If I is smaller than Ineigh, this means that we
           !! did not consider Ineigh yet and we did not treat the edge between
           !! the two triangles.
           Npoints=Npoints+1 ! we add a new point : the middle of the edge
           CoorInter(Npoints,1)=x
           CoorInter(Npoints,2)=y
           Node(5)=Npoints
           DO J=1,3
              IF (Neigh(INeigh,J).EQ.I) THEN
                 NeighInter(INeigh,J)=Ntri+1!! The two neighbourgh of Ineigh
                 !! through edge J are Ntri+1 and Ntri+3
                 NeighInter(INeigh,J+3)=Ntri+3
                 EXIT
              END IF
           END DO
        ELSE
          !! If we already handle this edge
           NeighInter(Ntri+3,2)=NeighInter(I,2) ! the first neighbour of Ntri+3
           NeighInter(Ntri+1,2)=NeighInter(I,5)! the first neighbour of Ntri+1
           DO J=1,3!! we run over all the edges of Ineigh to find which one is
              !! between I and Ineigh
              IF (Neigh(INeigh,J).EQ.I) THEN
                 NeighInter(NeighInter(I,2),J)=Ntri+3!! the Jth neighbor of NeighInter(I,2)
                 NeighInter(NeighInter(I,5),J)=Ntri+1!! the Jth neighbor of NeighInter(I,5)
                 SELECT CASE(J)
                 CASE(1)
                    Node(5)=TriInter(NeighInter(Ntri+3,2),2)!! since we
                    !! already handle the edge, its middle is already
                    !! constructed. It should be the second point of the second
                    !! neighbor of Ntri+3
                    IF (Node(5).NE.TriInter(NeighInter(Ntri+1,2),3)) THEN !!It
                       !!should also be the third point of the second
                    !! neighbor of Ntri+1
                       WRITE(6,*) 'error11'
                       STOP
                    END IF
                    IF (CoorInter(Node(5),1).NE.x) THEN
                       WRITE(6,*) 'error12'
                       STOP
                    END IF
                    IF (CoorInter(Node(5),2).NE.y) THEN
                       WRITE(6,*) 'error13'
                       STOP
                    END IF
                 CASE(2)
                    Node(5)=TriInter(NeighInter(Ntri+3,2),3)!! since we
                    !! already handle the edge, its middle is already
                    !! constructed. It should be the third point of the second
                    !! neighbor of Ntri+3
                    IF (Node(5).NE.TriInter(NeighInter(Ntri+1,2),1)) THEN!!It
                       !!should also be the first point of the second
                    !! neighbor of Ntri+1
                       WRITE(6,*) 'error14'
                       STOP
                    END IF
                 CASE(3)
                    Node(5)=TriInter(NeighInter(Ntri+3,2),1)!! since we
                    !! already handle the edge, its middle is already
                    !! constructed. It should be the first point of the second
                    !! neighbor of Ntri+3
                    IF (Node(5).NE.TriInter(NeighInter(Ntri+1,2),2)) THEN!!It
                       !!should also be the second point of the second
                    !! neighbor of Ntri+1
                       WRITE(6,*) 'error17',NeighInter(Ntri+3,2)
                       STOP
                    END IF
                    IF (CoorInter(Node(5),1).NE.x) THEN
                       WRITE(6,*) 'error18'
                       STOP
                    END IF
                    IF (CoorInter(Node(5),2).NE.y) THEN
                       WRITE(6,*) 'error19'
                       STOP
                    END IF
                 END SELECT
                 EXIT
              END IF
           END DO
        END IF
     ELSE
       !! If there is no neighbor, we are on the boundary. Obviously, we
        !! never handled this edge
        Npoints=Npoints+1!! we define a new point which is the center of the edge
        CoorInter(Npoints,1)=x
        CoorInter(Npoints,2)=y
        Node(5)=Npoints
        NeighInter(Ntri+1,2)=-1!there is no neighbor
        NeighInter(Ntri+3,2)=-1!there is no neighbor

        !!we find the global number of the edge we are considering
        !!we change the new value of bc_edge
        DO II=1,Nedge
        ! !        write(6,*) "loop",II, Nedge_temp
          IF(((Node(3).eq.Edge_pt(II,1)).and.(Node(1).eq.Edge_pt(II,2))).or.&
               & ((Node(3).eq.Edge_pt(II,2)).and.(Node(1).eq.Edge_pt(II,1))))THEN
          !           !!!this is the global number of the edge
                     exit
          END IF
        END DO
        bc_edge_local(6)=bc_edge(II)
        bc_edge_local(7)=bc_edge(II)
     END IF
     !        Edge 3 (12)
     x=Coor(Node(1),1)+1/2.D0*V12(1)
     y=Coor(Node(1),2)+1/2.D0*V12(2)
     !! x and y are the coordinate of the middle of edge 3
     Ineigh=Neigh(I,3)! we get the original neighbor of triangle I
     IF (Ineigh.NE.-1) THEN! if this triangle exists
        IF(I.LT.Ineigh) THEN!! If I is smaller than Ineigh, this means that we
           !! did not consider Ineigh yet and we did not treat the edge between
           !! the two triangles.
           Npoints=Npoints+1
           CoorInter(Npoints,1)=x
           CoorInter(Npoints,2)=y
           Node(6)=Npoints
           DO J=1,3
              IF (Neigh(INeigh,J).EQ.I) THEN
                 NeighInter(INeigh,J)=Ntri+2!! The two neighbourgh of Ineigh
                 !! through edge J are Ntri+2 and Ntri+1
                 NeighInter(INeigh,J+3)=Ntri+1
                 EXIT
              END IF
           END DO
        ELSE
          !! If we already handle this edge
           NeighInter(Ntri+1,3)=NeighInter(I,3) ! the first neighbour of Ntri+1
           NeighInter(Ntri+2,3)=NeighInter(I,6)! the first neighbour of Ntri+2
           DO J=1,3!! we run over all the edges of Ineigh to find which one is
              !! between I and Ineigh
            IF (Neigh(INeigh,J).EQ.I) THEN
                 NeighInter(NeighInter(I,3),J)=Ntri+1!! the Jth neighbor of NeighInter(I,3)
                 NeighInter(NeighInter(I,6),J)=Ntri+2!! the Jth neighbor of NeighInter(I,6)
                 SELECT CASE(J)
                 CASE(1)
                    Node(6)=TriInter(NeighInter(Ntri+1,3),2)!! since we
                    !! already handle the edge, its middle is already
                    !! constructed. It should be the second point of the third
                    !! neighbor of Ntri+1
                     IF (Node(6).NE.TriInter(NeighInter(Ntri+2,3),3)) THEN !!It
                       !!should also be the third point of the third
                    !! neighbor of Ntri+2
                       WRITE(6,*) 'error24'
                       STOP
                    END IF
                 CASE(2)
                    Node(6)=TriInter(NeighInter(Ntri+1,3),3)!! since we
                    !! already handle the edge, its middle is already
                    !! constructed. It should be the third point of the third
                    !! neighbor of Ntri+1
                    IF (Node(6).NE.TriInter(NeighInter(Ntri+2,3),1)) THEN !!It
                       !!should also be the first point of the third
                    !! neighbor of Ntri+2
                       WRITE(6,*) 'error24'
                       STOP
                    END IF
                    IF (CoorInter(Node(6),1).NE.x) THEN
                       WRITE(6,*) 'error25'
                       STOP
                    END IF
                    IF (CoorInter(Node(6),2).NE.y) THEN
                       WRITE(6,*) 'error26'
                       STOP
                    END IF
                 CASE(3)
                    Node(6)=TriInter(NeighInter(Ntri+1,3),1)!! since we
                    !! already handle the edge, its middle is already
                    !! constructed. It should be the first point of the third
                    !! neighbor of Ntri+1
                    IF (Node(6).NE.TriInter(NeighInter(Ntri+2,3),2)) THEN!!It
                       !!should also be the second point of the third
                       !! neighbor of Ntri+2
                       WRITE(6,*) 'error27'
                       STOP
                    END IF
                 END SELECT
                 EXIT
              END IF
           END DO
        END IF
     ELSE
       !! If there is no neighbor, we are on the boundary. Obviously, we
        !! never handled this edge
        Npoints=Npoints+1!! we define a new point which is the center of the edge
        CoorInter(Npoints,1)=x
        CoorInter(Npoints,2)=y
        Node(6)=Npoints
        NeighInter(Ntri+1,3)=-1!there is no neighbor
        NeighInter(Ntri+2,3)=-1!there is no neighbor

        !!we find the global number of the edge we are considering
        !!we change the new value of bc_edge
        DO II=1,Nedge
        ! !        write(6,*) "loop",II, Nedge_temp
          IF(((Node(1).eq.Edge_pt(II,1)).and.(Node(2).eq.Edge_pt(II,2))).or.&
               & ((Node(1).eq.Edge_pt(II,2)).and.(Node(2).eq.Edge_pt(II,1))))THEN
          !           !!!this is the global number of the edge
                     exit
          END IF
        END DO
        bc_edge_local(8)=bc_edge(II)
        bc_edge_local(9)=bc_edge(II)
     END IF
     TriInter(I,1)=Node(4)
     TriInter(I,2)=Node(5)
     TriInter(I,3)=Node(6)
     !!!!Element I is the central triangle composed of Node(4), Node(5), Node(6)

    ! degree_triInter(I)=degree_triInter(I)


     NeighInter(I,1)=Ntri+1
     NeighInter(I,2)=Ntri+2
     NeighInter(I,3)=Ntri+3
     !!! The three neighbors of I are the three new triangles Ntri+1, Ntri+2 and Ntri+3



     !! Element Ntri+1 is the triangle containing the original first node
     TriInter(Ntri+1,1)=Node(1)
     TriInter(Ntri+1,2)=Node(6)
     TriInter(Ntri+1,3)=Node(5)
     !!! all the neighbors of Ntri+1 have already been defined except the
     !!! central triangle I
     NeighInter(Ntri+1,1)=I
     degree_triInter(Ntri+1)=degree_triInter(I)

     !! Element Ntri+2 is the triangle containing the original first node
     TriInter(Ntri+2,1)=Node(6)
     TriInter(Ntri+2,2)=Node(2)
     TriInter(Ntri+2,3)=Node(4)
     !!! all the neighbors of Ntri+1 have already been defined except the
     !!! central triangle I
     NeighInter(Ntri+2,2)=I
     degree_triInter(Ntri+2)=degree_triInter(I)

     !! Element Ntri+3 is the triangle containing the original first node
     TriInter(Ntri+3,1)=Node(5)
     TriInter(Ntri+3,2)=Node(4)
     TriInter(Ntri+3,3)=Node(3)
     !!! all the neighbors of Ntri+1 have already been defined except the
     !!! central triangle I
     NeighInter(Ntri+3,3)=I
     degree_triInter(Ntri+3)=degree_tri(I)

     Ref_mediainter(I)=Ref_media(I)
     Ref_mediainter(Ntri+1)=Ref_media(I)
     Ref_mediainter(Ntri+2)=Ref_media(I)
     Ref_mediainter(Ntri+3)=Ref_media(I)
     Ntri=Ntri+3

     !!!construction of the edges
     !!!from the 6 nodes, we can construct 9 edges
     !!!in this edges we need to check if we did not add it before
     !!!we increase Nedge_temp of 9 - number edges already considered

     !!local numbers
     ! Edge   Nodes
     ! 1    4-6
     ! 2    5-4
     ! 3    5-6
     ! 4    2-4
     ! 5    4-3
     ! 6    3-5
     ! 7    5-1
     ! 8    1-6
     ! 9    6-2
     !!Edge_temp=
     Edge_temp(1,1)=Node(4)
     Edge_temp(1,2)=Node(6)
     Edge_temp(2,1)=Node(5)
     Edge_temp(2,2)=Node(4)
     Edge_temp(3,1)=Node(5)
     Edge_temp(3,2)=Node(6)

     Edge_temp(4,1)=Node(2)
     Edge_temp(4,2)=Node(4)
     Edge_temp(5,1)=Node(4)
     Edge_temp(5,2)=Node(3)
     Edge_temp(6,1)=Node(3)
     Edge_temp(6,2)=Node(5)

     Edge_temp(7,1)=Node(5)
     Edge_temp(7,2)=Node(1)
     Edge_temp(8,1)=Node(1)
     Edge_temp(8,2)=Node(6)
     Edge_temp(9,1)=Node(6)
     Edge_temp(9,2)=Node(2)

     DO Local_Edge=1,9
       DO II=1,Nedge_temp
  !        write(6,*) "loop",II, Nedge_temp
         IF(((Edge_ptInter(II,1).eq.Edge_temp(Local_Edge,1)).and.(Edge_ptInter(II,2).eq.Edge_temp(Local_Edge,2))).or.&
            & ((Edge_ptInter(II,1).eq.Edge_temp(Local_Edge,2)).and.(Edge_ptInter(II,2).eq.Edge_temp(Local_Edge,1))))THEN
            !!!the edge has already been considered
            exit
          END IF
       END DO
       IF(II.gt.Nedge_temp)THEN !! the edge has not been considered before, we must add it
  !       write(6,*) "comp",II, Nedge_temp
         Nedge_temp=Nedge_temp+1
         Edge_ptInter(Nedge_temp,1)= Edge_temp(Local_Edge,1)
         Edge_ptInter(Nedge_temp,2)= Edge_temp(Local_Edge,2)
         bc_edgeInter(Nedge_temp)=bc_edge_local(Local_Edge)
    !!     write(6,*) "bc_edge_local(Local_Edge)",bc_edge_local(Local_Edge)
          Length_edge_tmp=SQRT((CoorInter(Edge_ptInter(Nedge_temp,1),1)-CoorInter(Edge_ptInter(Nedge_temp,2),1))**2&
              &+(CoorInter(Edge_ptInter(Nedge_temp,1),2)-CoorInter(Edge_ptInter(Nedge_temp,2),2))**2)
          if(Length_edge_tmp.gt.Length_edge_max) Length_edge_max=Length_edge_tmp
       END IF
     END DO
  !    stop

    !!bc_edge???
  !!!  write(6,*) "bc_edge ??"
    !!!bc_edge(ABS(Edge_Tri(I_loc,Edge))).EQ.4) --> construit plus tard...
    !!!we can rebuild bc_edge from the beginning, using the conditions on the three old deg_edges
    !!! if   NeighInter(Ntri+1,3)=-1!there is no neighbor,  NeighInter(Ntri+2,3)=-1!there is no neighbor
    !!so we must know if the boundary condition is 3 or 4
    !!we have edge_pt, we know the two points of the three edges
    !!when we are on the edges, we search the old global number of the edge that we are considering
    !! id bc_edge is not equal to zero, we change bc_edge on the two new edges.

  END DO
  write(6,*) "Length_edge_max", Length_edge_max
  open(10,file="Length_edge_max.out")
  write(10,*) "Length_edge_max", Length_edge_max
  close(10)

  DEALLOCATE(Tri)
  DEALLOCATE(Neigh)
  DEALLOCATE(Edge_pt)
  DEALLOCATE(degree_tri)
  DEALLOCATE(bc_edge)
  !!DEALLOCATE(veloc)
  DEALLOCATE(Coor)
  DEALLOCATE(Ref_media)
  ALLOCATE(Tri(Ntri,3))
  ALLOCATE(Neigh(Ntri,3))
  ALLOCATE(Coor(NPoints,2))
  !!Nedge=2*Nedge+3*NTri !!!false in HDG !!!
  Nedge=Nedge_temp
  !  write(6,*) "Nedge", Nedge, Nedge_temp
  ! IF(Nedge.ne.Nedge_temp)THEN
  !   WRITE(6,*) 'Problem of numerotation on the edges in sub_refine'
  !   WRITE(6,*) 'Program stopping...'
  !   STOP
  ! END IF
  ALLOCATE(Edge_pt(Nedge,2))
  ALLOCATE(bc_edge(Nedge))
  ALLOCATE(Ref_media(Ntri))
  ALLOCATE(degree_tri(Ntri))
  Tri=0
  Neigh=0
  Edge_pt=0
  Coor=Coorinter(1:Npoints,:)
  DEALLOCATE(CoorInter)

  !!!! renumerotation
  Tri=TriInter
  Neigh=Neighinter(:,1:3)
  Edge_pt= Edge_ptInter
  Ref_media=Ref_mediainter
  degree_tri=degree_triInter
  bc_edge=bc_edgeInter
  DEALLOCATE(TriInter)
  DEALLOCATE(NeighInter)
  DEALLOCATE(Edge_ptInter)
  DEALLOCATE(Edge_temp)
  DEALLOCATE(bc_edge_local)
  DEALLOCATE(Ref_mediaInter)
  DEALLOCATE(degree_triInter)
  DEALLOCATE(bc_edgeInter)
END SUBROUTINE sub_refine
