! > compute some incircle radii and sphere

subroutine sub_ray_circle_ins(a, b, c, val)
  use precision
  implicit none
  real (kind=dp), intent (in) :: a(2), b(2), c(2)
  real (kind=dp), intent (out) :: val
  real (kind=dp) :: xlen(2)

  xlen(:) = 1.0d0
  call sub_ray_circle_ins_norm(a, b, c, xlen, val)
end subroutine

! > @todo : document this function more seriously
! > compute a normalized incircle radius
! >
! > the computation is done using the Heron's formula
! > \f$ R = \frac{\sqrt{s(s-a)(s-b)(s-c)}}{s} \mbox{ where } s = (a + b + c)/2
! \f$

subroutine sub_ray_circle_ins_norm(a, b, c, xlen, val)
  use :: precision
  implicit none
  real (kind=dp), intent (in) :: a(2), b(2), c(2), xlen(2)
  real (kind=dp), intent (out) :: val
  real (kind=dp) :: ab(2), ac(2), bc(2)
  real (kind=dp) :: long_ab, long_ac, long_bc, aire

  ab(:) = (b(:)-a(:))/xlen(:)
  ac(:) = (c(:)-a(:))/xlen(:)
  bc(:) = (c(:)-b(:))/xlen(:)

  long_ab = sqrt(sum(ab**2))
  long_ac = sqrt(sum(ac**2))
  long_bc = sqrt(sum(bc**2))

  aire = 1/2.*(long_ab+long_ac+long_bc)
  aire = aire*(aire-long_ab)*(aire-long_ac)*(aire-long_bc)
  aire = sqrt(aire)

  val = 2*aire/(long_ab+long_ac+long_bc)

end subroutine

subroutine sub_ray_triangle_norm(a, b, c, xlen, val)
  use :: precision
  implicit none
  real (kind=dp), intent (in) :: a(2), b(2), c(2), xlen(2)
  real (kind=dp), intent (out) :: val
  real (kind=dp) :: ab(2), ac(2), bc(2)
  real (kind=dp) :: long_ab, long_ac, long_bc, aire


  ab(:) = (b(:)-a(:))/xlen(:)
  ac(:) = (c(:)-a(:))/xlen(:)
  bc(:) = (c(:)-b(:))/xlen(:)

  long_ab = sqrt(sum(ab**2))
  long_ac = sqrt(sum(ac**2))
  long_bc = sqrt(sum(bc**2))

  aire = 1/2.*(long_ab+long_ac+long_bc)
  aire = aire*(aire-long_ab)*(aire-long_ac)*(aire-long_bc)
  aire = sqrt(aire)

  val = long_ab*long_ac*long_bc/(aire)*0.5
end subroutine

subroutine sub_ray_sphere_ins(a, b, c, d, val)
  use precision
  implicit none
  real (kind=dp), intent (in) :: a(3), b(3), c(3), d(3)
  real (kind=dp), intent (out) :: val
  real (kind=dp) :: xlen(3)

  xlen(:) = 1.0d0
  call sub_ray_sphere_ins_norm(a, b, c, d, xlen, val)
end subroutine

subroutine sub_ray_sphere_ins_norm(a, b, c, d, xlen, val)
  use precision
  implicit none
  real (kind=dp), intent(in) :: a(3), b(3), c(3), d(3), xlen(3)
  real (kind=dp), intent (out) :: val
  real (kind=dp) :: ab(3), ac(3), ad(3), bc(3), bd(3)
  real (kind=dp) :: abc(3), abd(3), acd(3), bcd(3)
  real (kind=dp) :: surf_abc, surf_abd, surf_acd, surf_bcd, aire, vol

  ab(:) = (b(:)-a(:))/xlen(:)
  ac(:) = (c(:)-a(:))/xlen(:)
  ad(:) = (d(:)-a(:))/xlen(:)
  bc(:) = (c(:)-b(:))/xlen(:)
  bd(:) = (d(:)-b(:))/xlen(:)

  abc(1) = ab(2)*bc(3) - ab(3)*bc(2)
  abc(2) = ab(3)*bc(1) - ab(1)*bc(3)
  abc(3) = ab(1)*bc(2) - ab(2)*bc(1)

  abd(1) = ab(2)*bd(3) - ab(3)*bd(2)
  abd(2) = ab(3)*bd(1) - ab(1)*bd(3)
  abd(3) = ab(1)*bd(2) - ab(2)*bd(1)

  acd(1) = ac(2)*ad(3) - ac(3)*ad(2)
  acd(2) = ac(3)*ad(1) - ac(1)*ad(3)
  acd(3) = ac(1)*ad(2) - ac(2)*ad(1)

  bcd(1) = bc(2)*bd(3) - bc(3)*bd(2)
  bcd(2) = bc(3)*bd(1) - bc(1)*bd(3)
  bcd(3) = bc(1)*bd(2) - bc(2)*bd(1)

  surf_abc = sqrt(sum(abc**2))/2
  surf_abd = sqrt(sum(abd**2))/2
  surf_acd = sqrt(sum(acd**2))/2
  surf_bcd = sqrt(sum(bcd**2))/2

  aire = (surf_abc+surf_abd+surf_acd+surf_bcd)
  vol = abs(dot_product(abc,ad))/6
  val = 3*vol/aire
end subroutine

subroutine sub_ray_tetra_norm(a, b, c, d, xlen, val)
  use precision
  implicit none
  real (kind=dp), intent (in) :: a(3), b(3), c(3), d(3), xlen(3)
  real (kind=dp), intent (out) :: val

  real (kind=dp) :: ab(3), ac(3), ad(3), cd(3), bd(3), bc(3)
  real (kind=dp) :: aa, bb, cc, aaa, bbb, ccc ! length ab, ac, ad, cd, bd, bc
                                              ! resp.
  real (kind=dp) :: abc(3), abd(3), acd(3), bcd(3)
  real (kind=dp) :: surf_abc, surf_abd, surf_acd, surf_bcd
  real (kind=dp) :: aire, vol6, p


  ab(:) = (b(:)-a(:))/xlen(:)
  ac(:) = (c(:)-a(:))/xlen(:)
  ad(:) = (d(:)-a(:))/xlen(:)
  bc(:) = (c(:)-b(:))/xlen(:)
  bd(:) = (d(:)-b(:))/xlen(:)
  cd(:) = (c(:)-d(:))/xlen(:)

  aa = sqrt(sum(ab**2))
  bb = sqrt(sum(ac**2))
  cc = sqrt(sum(ad**2))
  aaa = sqrt(sum(cd**2))
  bbb = sqrt(sum(bd**2))
  ccc = sqrt(sum(bc**2))

  p = 0.5*(aa*aaa+bb*bbb+cc*ccc)

  abc(1) = ab(2)*bc(3) - ab(3)*bc(2)
  abc(2) = ab(3)*bc(1) - ab(1)*bc(3)
  abc(3) = ab(1)*bc(2) - ab(2)*bc(1)

  abd(1) = ab(2)*bd(3) - ab(3)*bd(2)
  abd(2) = ab(3)*bd(1) - ab(1)*bd(3)
  abd(3) = ab(1)*bd(2) - ab(2)*bd(1)

  acd(1) = ac(2)*ad(3) - ac(3)*ad(2)
  acd(2) = ac(3)*ad(1) - ac(1)*ad(3)
  acd(3) = ac(1)*ad(2) - ac(2)*ad(1)

  bcd(1) = bc(2)*bd(3) - bc(3)*bd(2)
  bcd(2) = bc(3)*bd(1) - bc(1)*bd(3)
  bcd(3) = bc(1)*bd(2) - bc(2)*bd(1)

  surf_abc = sqrt(sum(abc**2))/2
  surf_abd = sqrt(sum(abd**2))/2
  surf_acd = sqrt(sum(acd**2))/2
  surf_bcd = sqrt(sum(bcd**2))/2

  aire = (surf_abc+surf_abd+surf_acd+surf_bcd)
  vol6 = abs(dot_product(abc,ad))  ! = 6*vol tetra
  val = sqrt(p*(p-aa*aaa)*(p-bb*bbb)*(p-cc*ccc))/vol6

end subroutine
