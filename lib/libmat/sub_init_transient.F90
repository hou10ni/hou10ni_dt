subroutine sub_init_transient
  USE precision
  USE data
  USE mesh
  USE mpi_modif
  use constant
  IMPLICIT NONE

  INTEGER :: I,J,II,iloc_tmp,deg_edges(Nfacesperelem)
  INTEGER :: K,tid,OMP_GET_THREAD_NUM,niter,imin

  ALLOCATE(openloc_flu(num_thread))
  ALLOCATE(limcla_flu(num_thread))
  ALLOCATE(openloc_sol(num_thread))
  ALLOCATE(openloc_poro(num_thread))
  ALLOCATE(openloc_em(num_thread))
  ALLOCATE(openloc_sem(num_thread))
  ALLOCATE(limcla_sol(num_thread))

  Iloc=0
  if (kind_of_medium == 0) then
     openloc_flu=0
     limcla_flu=nflusol+nflu
     !     CALL OMP_SET_NUM_THREADS(num_thread)
     !     !$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(I,K,J,TID,niter)
     DO II=1,Nflu_loc+Nflusol_loc
        TID = 0!OMP_GET_THREAD_NUM()
        limcla_flu(tid+1)=MIN(limcla_flu(tid+1),I)
           openloc_flu(tid+1)=openloc_flu(tid+1)+nphi(degree_tri_loc(ii))**2
           do K=1,Nfacesperelem
              J=Neigh_loc(II,K)
              IF (J.GT.0) THEN
                 IF(acouela(ref_media_loc(J)).EQ.1) THEN
                    openloc_flu(tid+1)=openloc_flu(tid+1)+nphi(degree_tri_loc(ii))&
                         &*nphi(degree_tri_loc(j))
                 END IF
              ENDIF
           enddo
     END DO
     !    !$OMP  END PARALLEL DO

  iloc_tmp=0
  iloc=iloc+SUM(openloc_flu)

  DO i=num_thread,1,-1
     openloc_flu(i)=iloc_tmp+SUM(openloc_flu(1:i-1))
  END DO
  imin=1

  if (is_cla) then
     DO j=1,num_thread
        loop_flu:         DO i=imin,Nbcla_flu
           IF(limcla_flu(j).LE.tricla_flu(I)) THEN
              limcla_flu(j)=I
              imin=i
              EXIT loop_flu
           END IF
        END DO loop_flu
        limcla_flu(j)=MIN(limcla_flu(j),nbcla_flu)
     END DO
  end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!
!!!!!!!!!!!!!! On calcule les Ã©lÃ©ments non nuls dans le milieu 2
!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     openloc_sol=0
     limcla_sol=nsolflu_loc+nsol_loc
     CALL OMP_SET_NUM_THREADS(num_thread)
     !$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(I,K,J,TID,niter)
     DO II=1,Nsolflu_loc+Nsol_loc
        I=II+Nflu_loc+Nflusol_loc
        TID = OMP_GET_THREAD_NUM()
        limcla_sol(tid+1)=MIN(limcla_sol(tid+1),II)
           openloc_sol(tid+1)=openloc_sol(tid+1)+dim**2*nphi(degree_tri_loc(i))**2
           DO K=1,Nfacesperelem
              J=Neigh_loc(I,K)
              IF (J.GT.0) THEN
                 IF(acouela(ref_media_loc(J)).EQ.2) THEN
                    openloc_sol(tid+1)=openloc_sol(tid+1)+dim**2*nphi(degree_tri_loc(i))*nphi(degree_tri_loc(j))
                 ENDIF
              END IF
           ENDDO 
     END DO
     !     !$OMP  END PARALLEL DO
     
  iloc_tmp=iloc
  iloc=iloc+SUM(openloc_sol)
  DO i=num_thread,1,-1
     openloc_sol(i)=iloc_tmp+SUM(openloc_sol(1:i-1))
  END DO
  imin=1
  if (is_cla) then
     DO j=1,num_thread
        loop_sol:         DO i=imin,Nbcla_sol
           IF(limcla_sol(j).LE.tricla_sol(I)-Nsolflu_loc-Nflu_loc) THEN
              limcla_sol(j)=I
              imin=i
              EXIT loop_sol
           END IF
        END DO loop_sol
        limcla_sol(j)=MIN(limcla_sol(j),nbcla_sol)
     END DO
  end if
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!
!!!!!!!!!!!! On termine par les interactions entre les deux milieux
!!!!!!!!!!!!
!!!!!!!!!!!!En cas de problÃ¨me, penser Ã  tester si le nombre d'Ã©lÃ©ments
!!!!!!!!!!!!non nuls de la premiÃ¨re boucle est Ã©gal Ã  celui de la deuxiÃ¨me
!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  DO II=1,Nflusol_loc
     I=Nflu_inner+II
        DO K=1,Nfacesperelem
           J=Neigh_loc(I,K)
           !!! If the Neighbor is in the solid part
           IF(J.GT.0) THEN
              IF(acouela(ref_media_loc(J)).EQ.2) THEN
                    Iloc=Iloc+2*dim*nphi(degree_tri_loc(i))*nphi(degree_tri_loc(j))
              END IF
           END IF
        END DO
  END DO

  !!TODO
!!$!! Condition compliquÃ©e, on verra plus tard
end if ! if (kind_of_medium == 0)


  if (kind_of_medium == 1) then
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!! electro !!!!!!!!!!!!!!!!!!!!!!!!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       openloc_em=0
       CALL OMP_SET_NUM_THREADS(num_thread)
       DO II=1,Nem_loc
          I=Triloc2glob(II)
          TID = OMP_GET_THREAD_NUM()
             IF (dim.EQ.2) THEN
                !! nombre d'elements non nuls maxi = Ntri*(nb_aretes_par_triangle*nb_ddl_arete*2)**2
                DO K=1,Nfacesperelem
                   deg_edges(K)=degree_edge(ABS(Edge_Tri(I,K)))
                ENDDO
                openloc_em(tid+1)=openloc_em(tid+1)+&
                     & (1*(Nphi_face(deg_edges(1))+Nphi_face(deg_edges(2))+Nphi_face(deg_edges(3))))**2
             ELSE
                 DO K=1,Nfacesperelem
                    deg_edges(K)=degree_face(ABS(Connec_tetra_face(I,K)))
                 ENDDO
                 openloc_em(tid+1)=openloc_em(tid+1)+&
                      & (3*(Nphi_face(deg_edges(1))+Nphi_face(deg_edges(2))+Nphi_face(deg_edges(3))+Nphi_face(deg_edges(4))))**2 !??
             ENDIF
       END DO
       !     !$OMP  END PARALLEL DO
    iloc_tmp=iloc
    iloc=iloc+SUM(openloc_em)
    DO i=num_thread,1,-1
       openloc_em(i)=iloc_tmp+SUM(openloc_em(1:i-1))
    END DO
    end if  ! if (kind_of_medium == 1)
  end subroutine sub_init_transient


