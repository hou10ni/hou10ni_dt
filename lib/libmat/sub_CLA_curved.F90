subroutine sub_CLA_curved
  use precision
  use data
  use mesh
  use POLYNOM
  use BASIS_FUNCTIONS
  use Matrix
  use curved2
  use constant
  use transient
  use MPI_modif
  use tools, only : compute_v_2d
  implicit none

  integer :: I,INeigh,Node(3),J,K,L,Edge,NbnoCla,J1,J2,deg,isol&
       ,tmp_flu,tmp_sol
  real(kind=dp) :: V(3,2),TestCla,d,val
  real(kind=dp),allocatable :: mat_aux(:,:)
  real(kind=dp) :: pt_courbe_x(Order_curved+1),pt_courbe_y(Order_curved+1)
  real(kind=dp) :: angle,kappa,co,si,coeffi,a,b,c,nx,ny,xg,yg,zg,xprime&
       &,yprime,xsecond,ysecond,curv,norme,vp,vs1,vs2
  real(kind=dp) :: coeff_pml_x,coeff_pml_y,coeff_pml_z &
       &,res,res1,res2,res3,res4,resxx,resxy,resyy
  real(kind=dp) :: A_cla,coeff_cla,theta_cla,gamma_cla,zeta_cla, kappa_cla,&
       & Denominator_cla, Numerator_cla

  write(6,*) 'DOES sub_CLA_curved.F90 EVER RUN ?'


  NBcla=0
  NBcla_flu=0
  NBcla_sol=0
  do I=1,Ntri_loc
     do J=1,nfacesperelem
        if (Neigh_loc(I,J)==-3) then
           NbCLA=NbCLA+1
           if(acouela(ref_media_loc(I)).eq.1) then
              nbcla_flu=nbcla_flu+1
              tmp_flu=tmp_flu+Nphi(degree_tri_loc(I))**2
           else 
              nbcla_sol=nbcla_sol+1
              tmp_sol=tmp_sol+dim**2*Nphi(degree_tri_loc(I))**2
           end if
           exit
        end if
     enddo
  enddo

  allocate(TriCla_flu(NbCLA_flu))
  allocate(TriCla_sol(NbCLA_sol))
  !allocate(Clatri(Ntri))
  allocate(BCla_flu(NbCla_flu))
  allocate(BCla_xx(NbCla_sol))
  allocate(BCla_yx(NbCla_sol))
  allocate(BCla_xy(NbCla_sol))
  allocate(BCla_yy(NbCla_sol))
  allocate(BCla_xx_tti(NbCla_sol))
  allocate(BCla_yx_tti(NbCla_sol))
  allocate(BCla_xy_tti(NbCla_sol))
  allocate(BCla_yy_tti(NbCla_sol))
     allocate(idx_mat_cla_flu(NbCla_flu+1))
     allocate(idx_mat_cla_sol(Nbcla_sol+1))
     idx_mat_cla_flu(1)=1
     idx_mat_cla_sol(1)=1
     allocate(A_CLA_flu(tmp_flu))
     allocate(A_CLA_sol(tmp_sol))
     A_CLA_flu=0.
     A_CLA_sol=0.
     allocate(mat_aux(dim*nphi(degree_max),dim*nphi(degree_max)))
  NbCla=0
  NbCla_flu=0
  NbCla_sol=0
  NbCla=0
  NbnoCla=0
  do I=1,NTri_loc
     Node = Tri_loc(I,:)
     ! FIXME : check that is necessary or not
     !!Computation of vectors V12,V23 and V31
     v = compute_v_2d( coor, node )

     coeff_pml_y=1._dp
     coeff_pml_x=1._dp

     TestCla=0
     do Edge=1,Nfacesperelem
!!! Searching Neighbor
        INeigh=Neigh_loc(I,Edge)
!!! If there is no Neighbor and the edge carries a ABC
        if(Ineigh.eq.-3) then
           if (TestCla==0) then
              TestCla=1
              NbCla=NbCla+1
              if(acouela(ref_media_loc(I)).eq.1) then
                 nbcla_flu=nbcla_flu+1
                 TriCla_flu(NbCla_flu)=I
                    tmp_flu=tmp_flu+Nphi(degree_tri_loc(I))**2
                    idx_mat_cla_flu(Nbcla_flu+1)=tmp_flu+1
              else 
                 nbcla_sol=nbcla_sol+1
                 TriCla_sol(NbCla_sol)=I

                    tmp_sol=tmp_sol+dim**2*Nphi(degree_tri_loc(I))**2
                    idx_mat_cla_sol(Nbcla_sol+1)=tmp_sol+1

                 if(dim.eq.2) then
                    Isol=I-Nflu_loc-Nflusol_loc

                    if(abs(Cij(Isol,1,3)+Cij(Isol,2,3)).gt.1e-8) then
                       angle= acos((Cij(Isol,1,1)-Cij(Isol,2,2))/2. &
                            &/sqrt((Cij(Isol,1,3)+Cij(Isol,2,3))**2 +(Cij(Isol &
                            &,1,1)-Cij(Isol,2,2))**2/4.))/2.
                    else
                       angle=0.
                    end if
                    kappa=sqrt((cos(angle)**4*Cij(Isol,1,1)+sin(angle)**4*Cij(Isol,2 &
                         &,2)+2.*cos(angle)**2 *sin(angle)**2*(2.*Cij(Isol,3,3) &
                         &+Cij(Isol,1,2))+4.*cos(angle)**3 *sin(angle)*Cij(Isol,1,3) &
                         &+4.*cos(angle) *sin(angle)**3*Cij(Isol,2,3))/(sin(angle)**4&
                         & *Cij(Isol,1,1)+cos(angle)**4*Cij(Isol,2,2)+2.*cos(angle)&
                         &**2 *sin(angle)**2*(2.*Cij(Isol,3,3)+Cij(Isol,1,2))-4. &
                         &*sin(angle)**3 *cos(angle)*Cij(Isol,1,3)-4.*sin(angle) &
                         &*cos(angle)**3*Cij(Isol,2,3)))
                    vp=sqrt((sin(angle)**4*Cij(Isol,1,1)+cos(angle)**4*Cij(Isol &
                         &,2,2)+2.*cos(angle)**2 *sin(angle)**2*(2.*Cij(Isol,3,3)&
                         & +Cij(Isol,1,2))-4.*sin(angle)**3 *cos(angle)*Cij(Isol&
                         & ,1,3)-4.*sin(angle) *cos(angle)**3*Cij(Isol,2,3)) &
                         &/rho(I))
                    vs1=sqrt((cos(angle)**2*sin(angle)**2*(Cij(Isol,1,1)+Cij(Isol&
                         & ,2,2))+(cos(angle)**2 -sin(angle)**2)**2*Cij(Isol,3,3)&
                         & -2.*cos(angle)* sin(angle)*(cos(angle)**2-sin(angle)&
                         &**2) *(Cij(Isol,1,3)-Cij(Isol,2,3))-2.*cos(angle)**2 &
                         &*sin(angle)**2*Cij(Isol,1,2))/rho(I))
                 else 
                    Isol=I-Nflu_loc-Nflusol_loc
                    vp=sqrt(Cij(Isol,1,1)/rho(I))
                    vs1=sqrt(Cij(Isol,4,4)/rho(I))
                    vs2=sqrt(Cij(Isol,4,4)/rho(I))
                 end if
              end if
           end if
           !!Computation of vectors V12,V23 and V31
           !! @todo : use compute_v_2d
           V(1,1)=Coor(Node(3),1)-Coor(Node(2),1)
           V(1,2)=Coor(Node(3),2)-Coor(Node(2),2)
           V(2,1)=Coor(Node(1),1)-Coor(Node(3),1)
           V(2,2)=Coor(Node(1),2)-Coor(Node(3),2)
           V(3,1)=Coor(Node(2),1)-Coor(Node(1),1)
           V(3,2)=Coor(Node(2),2)-Coor(Node(1),2)
           select case(Edge)
           case(1)
              J1=2
              J2=3
           case(2)
              J1=3
              J2=1
           case(3)
              J1=1
              J2=2
           end select
           pt_courbe_x(1)=Coor(Node(J1),1)
           pt_courbe_x(order_curved+1)=Coor(Node(J2),1)
           pt_courbe_y(1)=Coor(Node(J1),2)
           pt_courbe_y(order_curved+1)=Coor(Node(J2),2)



           call sub_def_pt_courbe_1d(pt_courbe_x,pt_courbe_y,Triloc2glob(I),Edge)


           deg=degree_tri_loc(I)
           if(acouela(ref_media_loc(I)).eq.1) then
                 !! TODO : multiplication by Minv curved
           else 
              !! [u][v]

              Isol=I-Nflu-Nflusol
              do J=1,nphi(deg)
                 do K=1,nphi(deg)
                    resxx=0D0
                    resxy=0D0
                    resyy=0D0
                    res1=0D0
                    res2=0D0
                    res3=0D0
                    res4=0D0
                    do L=1,NGL1D
                       ny=-sum(pt_courbe_x*value_basis_curved_1D%value_volume(1)&
                            &%matphii_ptgl(2)%coeff(L,:,1))
                       nx=sum(pt_courbe_y*value_basis_curved_1D%value_volume(1)&
                            &%matphii_ptgl(2)%coeff(L,:,1))
                       norme=sqrt(nx**2+ny**2)
                       co=cos(angle)*nx+sin(angle)*ny
                       si=-cos(angle)*ny+sin(angle)*nx

                       a=(kappa*co**2+si**2)**2/sqrt(kappa**2*co**2+si**2)
                       b=(kappa*co**2+si**2)*(kappa-1.)*co*si/sqrt(kappa**2*co**2+si**2)
                       c=(kappa*co**2+si**2)*(kappa-1.)*co*si/sqrt(kappa**2*co**2+si**2)
                       d=(kappa-1.)**2*co**2*si**2/sqrt(kappa**2*co**2+si**2)
                       val=value_basis%value_surface(deg)&
                            &%matphii_ptgl_surf(1)%coeff(L,J,1,Edge)*value_basis&
                            &%value_surface(deg)%matphii_ptgl_surf(1)&
                            &%coeff(L,K,1,Edge)*wGL1D(L)
                          write(6,*) 'todo abc for curved boundaries in time domain'
                          stop
                    end do
                 end do
              end do
           end if
        end if
        if (TestCla==0) then
           NbnoCla=NbnoCla+1
        end if
     end do

  end do

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!    
!!!!!!!!! Computation of Minv*C
!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!TODO
!!$IF (.not. helmholtz.EQ.0) THEN
!!$DO I=1,Nbcla
!!$   Bcla(I,1:Nphi,1:Nphi)=MATMUL(Minv,Bcla(I,1:Nphi,1:Nphi))/DFVec(TriCla(I))*mu(TriCla(I))
!!$ENDDO
!!$END IF

end subroutine sub_CLA_curved
