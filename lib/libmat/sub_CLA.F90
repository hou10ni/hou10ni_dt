subroutine sub_CLA
  use precision
  
  use mesh
  use data
  use matrix
  use constant
  use transient
  implicit none

  integer :: INeigh,Node(nverticesperelem),J,JJ,NbnoCla,Isol,II&
       &,tmp_flu,tmp_sol,phiface(nverticesperface,nfacesperelem)
  real(kind=dp) :: V(dim,dim),TestCla,xg,yg,zg,normal(dim),dfvec,norm2d
  real(kind=dp),allocatable :: mat_aux(:,:)
  real(kind=dp) :: angle,kappa,co,si,coeffi,a,b,c,d,nx,ny,nz,vp,vs1,vs2
  real(kind=dp) :: angle2,co2,si2,ax,bx,cx,ay,by,cy,az,bz,cz
  complex(kind=dp) :: coeff_pml_x,coeff_pml_y,coeff_pml_z
  complex(kind=dp) :: A_cla,iomega,coeff_cla,theta_cla,gamma_cla,zeta_cla, kappa_cla,&
       & Denominator_cla, Numerator_cla

  if(DIM.eq.2) then
!!!!!!!!!!!!!!!!!  Local numerotation of vertices on each edge:
     !Functions on Edge 23
     Phiface(1,1)=2
     Phiface(2,1)=3
     !Functions on Edge 31
     Phiface(1,2)=3
     Phiface(2,2)=1
     !Functions on Edge 12
     Phiface(1,3)=1
     Phiface(2,3)=2
  else 
!!!!!!!!!!!!!!!!!  Local numerotation of vertices on each edge:
     !Functions on Face 1
     Phiface(1,1)=2
     Phiface(2,1)=4
     Phiface(3,1)=3
     !Functions on Face 2
     Phiface(1,2)=1
     Phiface(2,2)=3
     Phiface(3,2)=4
     !Functions on Face 3
     Phiface(1,3)=1
     Phiface(2,3)=4
     Phiface(3,3)=2
     !Functions on Face 4
     Phiface(1,4)=1
     Phiface(2,4)=2
     Phiface(3,4)=3
  end if

  NBcla=0
  NBcla_flu=0
  NBcla_sol=0
  tmp_flu=0
  tmp_sol=0

  do II=1,Ntri_loc
     do J=1,nfacesperelem
        if (Neigh_loc(II,J)==-3) then
           NbCLA=NbCLA+1
           if(acouela(ref_media_loc(II)).eq.1) then
              nbcla_flu=nbcla_flu+1
              tmp_flu=tmp_flu+Nphi(degree_tri_loc(II))**2
           else 
              nbcla_sol=nbcla_sol+1
              tmp_sol=tmp_sol+dim**2*Nphi(degree_tri_loc(II))**2
           end if
           exit
        end if
     enddo
  enddo

  allocate(TriCla_flu(NbCLA_flu))
  allocate(TriCla_sol(NbCLA_sol))
  !allocate(Clatri(Ntri))
  allocate(BCla_flu(NbCla_flu))
  allocate(BCla_sol(NbCla_sol))
  allocate(BCla_xx(NbCla_sol))
  allocate(BCla_yx(NbCla_sol))
  allocate(BCla_xy(NbCla_sol))
  allocate(BCla_yy(NbCla_sol))
  allocate(BCla_xx_tti(NbCla_sol))
  allocate(BCla_yx_tti(NbCla_sol))
  allocate(BCla_xy_tti(NbCla_sol))
  allocate(BCla_yy_tti(NbCla_sol))

  allocate(idx_mat_cla_flu(NbCla_flu+1))
  allocate(idx_mat_cla_sol(Nbcla_sol+1))
  idx_mat_cla_flu(1)=1
  idx_mat_cla_sol(1)=1
  allocate(A_CLA_flu(tmp_flu))
  allocate(A_CLA_sol(tmp_sol))
  A_CLA_flu=0.
  A_CLA_sol=0.
  allocate(mat_aux(dim*nphi(degree_max),dim*nphi(degree_max)))

  NbCla=0
  NbCla_flu=0
  NbCla_sol=0
  tmp_flu=0
  tmp_sol=0

  !----------------------------------------------------------------------------
  !*Loop on all the elements 
  !----------------------------------------------------------------------------
  do II=1,Ntri_loc

     Node = Tri_loc(II,:)
     !!Computation of vectors V12,V23 and V31

     TestCla=0

     !----------------------------------------------------------------------------
     !**Loop on the edges(2D)/faces(3D) of the current element
     !----------------------------------------------------------------------------
     do JJ=1,Nfacesperelem

        INeigh=Neigh_loc(II,JJ)

        !----------------------------------------------------------------------------
        !***If there is no neighbour and the edge/face carries an ABC 
        !----------------------------------------------------------------------------
        if(Ineigh.eq.-3) then

           !----------------------------------------------------------------------------
           !****If this is the first time that we compute an ABC for the current element 
           !----------------------------------------------------------------------------
           if (TestCla==0) then

              TestCla=1
              NbCla=NbCla+1

              !----------------------------------------------------------------------------
              !*****If the current element is in the fluid domain 
              !----------------------------------------------------------------------------
              if(acouela(ref_media_loc(II)).eq.1) then

                 nbcla_flu=nbcla_flu+1
                 TriCla_flu(NbCla_flu)=II


                 tmp_flu=tmp_flu+Nphi(degree_tri_loc(II))**2
                 idx_mat_cla_flu(Nbcla_flu+1)=tmp_flu+1

                 !----------------------------------------------------------------------------
                 !*****If the current element is in the solid domain 
                 !----------------------------------------------------------------------------
              else 
                 nbcla_sol=nbcla_sol+1
                 TriCla_sol(NbCla_sol)=II
                 tmp_sol=tmp_sol+dim**2*Nphi(degree_tri_loc(II))**2
                 idx_mat_cla_sol(Nbcla_sol+1)=tmp_sol+1



                 !----------------------------------------------------------------------------
                 !******If the dim is 2
                 !----------------------------------------------------------------------------
                 if(dim.eq.2) then
                    Isol=II-Nflu_loc-Nflusol_loc

                    if(abs(Cij(Isol,1,3)+Cij(Isol,2,3)).gt.1e-8) then
                       angle= acos((Cij(Isol,1,1)-Cij(Isol,2,2))/2. &
                            &/sqrt((Cij(Isol,1,3)+Cij(Isol,2,3))**2 +(Cij(Isol &
                            &,1,1)-Cij(Isol,2,2))**2/4.))/2.
                    else
                       angle=0.
                    end if
                    kappa=sqrt((cos(angle)**4*Cij(Isol,1,1)+sin(angle)**4*Cij(Isol,2 &
                         &,2)+2.*cos(angle)**2 *sin(angle)**2*(2.*Cij(Isol,3,3) &
                         &+Cij(Isol,1,2))+4.*cos(angle)**3 *sin(angle)*Cij(Isol,1,3) &
                         &+4.*cos(angle) *sin(angle)**3*Cij(Isol,2,3))/(sin(angle)**4&
                         & *Cij(Isol,1,1)+cos(angle)**4*Cij(Isol,2,2)+2.*cos(angle)&
                         &**2 *sin(angle)**2*(2.*Cij(Isol,3,3)+Cij(Isol,1,2))-4. &
                         &*sin(angle)**3 *cos(angle)*Cij(Isol,1,3)-4.*sin(angle) &
                         &*cos(angle)**3*Cij(Isol,2,3)))
                    vp=sqrt((sin(angle)**4*Cij(Isol,1,1)+cos(angle)**4*Cij(Isol &
                         &,2,2)+2.*cos(angle)**2 *sin(angle)**2*(2.*Cij(Isol,3,3)&
                         & +Cij(Isol,1,2))-4.*sin(angle)**3 *cos(angle)*Cij(Isol&
                         & ,1,3)-4.*sin(angle) *cos(angle)**3*Cij(Isol,2,3)) &
                         &/rho(II))
                    vs1=sqrt((cos(angle)**2*sin(angle)**2*(Cij(Isol,1,1)+Cij(Isol&
                         & ,2,2))+(cos(angle)**2 -sin(angle)**2)**2*Cij(Isol,3,3)&
                         & -2.*cos(angle)* sin(angle)*(cos(angle)**2-sin(angle)&
                         &**2) *(Cij(Isol,1,3)-Cij(Isol,2,3))-2.*cos(angle)**2 &
                         &*sin(angle)**2*Cij(Isol,1,2))/rho(II))

                    !----------------------------------------------------------------------------
                    !******If the dim is 3
                    !----------------------------------------------------------------------------
                 else 

                    Isol=II-Nflu_loc-Nflusol_loc
                    vp=sqrt(Cij(Isol,1,1)/rho(II))
                    vs1=sqrt(Cij(Isol,4,4)/rho(II))
                    vs2=sqrt(Cij(Isol,4,4)/rho(II))
                 end if

              end if !! End IF Test to know where we are : fluid or solid

              !----------------------------------------------------------------------------
              !*****If Helmholtz
              !----------------------------------------------------------------------------


                 if(dim.eq.2) then
                    V(1,:)=Coor(Tri_loc(II,2),:)-Coor(Tri_loc(II,1),:)
                    V(2,:)=Coor(Tri_loc(II,3),:)-Coor(Tri_loc(II,1),:)
                    DFVEC=V(1,1)*V(2,2)-V(1,2)*V(2,1)
                 else 
                    V(1,:)=Coor(Tri_loc(II,2),:)-Coor(Tri_loc(II,1),:)
                    V(2,:)=Coor(Tri_loc(II,3),:)-Coor(Tri_loc(II,1),:)
                    V(3,:)=Coor(Tri_loc(II,4),:)-Coor(Tri_loc(II,1),:)
                    !@todo : use mixed 
                    DFVEC=-(V(2,2)*V(1,3)-V(2,3)*V(1,2))*V(3,1)-(V(2,3)*V(1,1)&
                         &-V(2,1)*V(1,3))&
                         &*V(3,2)-(V(2,1)*V(1,2)-V(2,2)*V(1,1))*V(3,3)
                 end if

           end if !! End IF This was the first time that we compute an ABC for the current element

           !----------------------------------------------------------------------------
           !****Computation of the coordinates of the normal vector of the edge/face of the current element
           !----------------------------------------------------------------------------
           if(dim.eq.2) then
              normal(1)=Coor(Tri_loc(II,Phiface(2,JJ)),2)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),2)
              normal(2)=Coor(Tri_loc(II,Phiface(1,JJ)),1)-Coor(Tri_loc(II&
                   &,Phiface(2,JJ)),1)
           else 
              normal(1)=(Coor(Tri_loc(II,Phiface(3,JJ)),2)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),2))*&
                   &(Coor(Tri_loc(II,Phiface(2,JJ)),3)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),3))-&
                   &(Coor(Tri_loc(II,Phiface(3,JJ)),3)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),3))*&
                   &(Coor(Tri_loc(II,Phiface(2,JJ)),2)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),2))
              normal(2)=(Coor(Tri_loc(II,Phiface(3,JJ)),3)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),3))*&
                   &(Coor(Tri_loc(II,Phiface(2,JJ)),1)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),1))-&
                   &(Coor(Tri_loc(II,Phiface(3,JJ)),1)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),1))*&
                   &(Coor(Tri_loc(II,Phiface(2,JJ)),3)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),3))
              normal(3)=(Coor(Tri_loc(II,Phiface(3,JJ)),1)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),1))*&
                   &(Coor(Tri_loc(II,Phiface(2,JJ)),2)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),2))-&
                   &(Coor(Tri_loc(II,Phiface(3,JJ)),2)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),2))*&
                   &(Coor(Tri_loc(II,Phiface(2,JJ)),1)-Coor(Tri_loc(II&
                   &,Phiface(1,JJ)),1))
           end if

           !----------------------------------------------------------------------------
           !****If the element is in the fluid domain
           !----------------------------------------------------------------------------
           if(acouela(ref_media_loc(II)).eq.1) then



                 A_CLA_flu(tmp_flu-Nphi(degree_tri_loc(II))**2+1:tmp_flu)&
                      &=A_CLA_flu(tmp_flu-Nphi(degree_tri_loc(II))**2&
                      &+1:tmp_flu)+reshape(matmul(Minv(degree_tri_loc(II))&
                      &%coeff*sqrt(mu(II))/sqrt(rho(II))/DFVEC,matrices &
                      &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)%coeff(1,1 &
                      &,:,: ,JJ)*sqrt(sum(normal**2))), (/Nphi(degree_tri_loc(II))&
                      &**2/))


              !----------------------------------------------------------------------------
              !****If the element is in the solid domain
              !----------------------------------------------------------------------------
           else 

              !----------------------------------------------------------------------------
              !*****If 2D
              !----------------------------------------------------------------------------
              if(dim.eq.2) then
                 nx=normal(1)/sqrt(sum(normal**2))
                 ny=normal(2)/sqrt(sum(normal**2))
                 co=cos(angle)*nx+sin(angle)*ny
                 si=-cos(angle)*ny+sin(angle)*nx
                 a=(kappa*co**2+si**2)**2/sqrt(kappa**2*co**2+si**2)
                 b=(kappa*co**2+si**2)*(kappa-1.)*co*si/sqrt(kappa**2*co**2&
                      &+si&
                      &**2)
                 c=(kappa*co**2+si**2)*(kappa-1.)*co*si/sqrt(kappa**2*co**2&
                      &+si&
                      &**2)
                 d=(kappa-1.)**2*co**2*si**2/sqrt(kappa**2*co**2+si**2)


                    coeffi=a*nx**2+d*ny**2-(b+c)*nx*ny
                    mat_aux(1:Nphi(degree_tri_loc(II))&
                         &,1:Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp&
                         &+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(2)**2/sqrt(sum(normal**2))*Vs1)


                    coeffi=d*nx**2+a*ny**2+(b+c)*nx*ny

                    mat_aux(Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II))&
                         &,Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(1)**2/sqrt(sum(normal**2))*Vs1)




                    coeffi=c*nx**2-b*ny**2+(a-d)*nx*ny

                    mat_aux(1:Nphi(degree_tri_loc(II))&
                         &,Nphi(degree_tri_loc(II))+1:2&
                         &*Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(1)*normal(2)/sqrt(sum(normal**2))&
                         &*Vs1)


                    coeffi=b*nx**2-c*ny**2+(a-d)*nx*ny
                    mat_aux(Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II))&
                         &,1:Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(1)*normal(2)/sqrt(sum(normal**2))&
                         &*Vs1)

                    mat_aux(1:Nphi(degree_tri_loc(II))&
                         &,1:2*Nphi(degree_tri_loc(II)))=&
                         &MATMUL(Minv(degree_tri_loc(II))%coeff,mat_aux(1:Nphi(degree_tri_loc(II))&
                         &,1:2*Nphi(degree_tri_loc(II))))/DFVec/rho(II)


                    mat_aux(Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II))&
                         &,1:2*Nphi(degree_tri_loc(II)))=&
                         &MATMUL(Minv(degree_tri_loc(II))%coeff&
                         &,mat_aux(Nphi(degree_tri_loc(II))+1:2&
                         &*Nphi(degree_tri_loc(II)),1:2&
                         &*Nphi(degree_tri_loc(II))))/DFVec/rho(II)

                    A_CLA_sol(tmp_sol-dim**2*Nphi(degree_tri_loc(II))**2+1:tmp_sol)&
                         &=A_CLA_sol(tmp_sol-dim**2*Nphi(degree_tri_loc(II))**2&
                         &+1:tmp_sol)+&
                         &RESHAPE( mat_aux(1:dim*Nphi(degree_tri_loc(II))&
                         &,1:dim*Nphi(degree_tri_loc(II))),&
                         &(/dim**2*Nphi(degree_tri_loc(II))**2/))


                 !----------------------------------------------------------------------------
                 !*****If 3D
                 !----------------------------------------------------------------------------
              else 
                 angle=0!pi/180._dp*30._dp !!! theta
                 angle2=0!pi/180._dp*15._dp !!! phi
                 kappa=1!SQRT(1._dp+2._dp*0.25)
                 Vp=sqrt(Cij(II-Nflu_loc-Nflusol_loc,1,1)/rho(II))
                 Vs1=sqrt(Cij(II-Nflu_loc-Nflusol_loc,6,6)/rho(II))
                 nx=normal(1)/sqrt(sum(normal**2))
                 ny=normal(2)/sqrt(sum(normal**2))
                 nz=normal(3)/sqrt(sum(normal**2))
                 norm2d=sqrt(nx**2+ny**2)

                 if(norm2d.gt.1d-6) then
                    co=(-nx*nz/norm2d*cos(angle2)-ny*nz/norm2d*sin(angle2))&
                         &*sin(angle)+norm2d*cos(angle)
                 else 
                    co=-nz*cos(angle2)*sin(angle)
                 end if
                 si=sqrt(1-co**2)
                 if(abs(si).gt.1d-6) then
                    if(norm2d.gt.0) then
                       co2=(nx*cos(angle2)+ny*sin(angle2))*sin(angle)+nz&
                            &*cos(angle)
                       si2=(-ny*cos(angle2)/norm2d+nx*sin(angle2)/norm2d)&
                            &*sin(angle)
                    else 
                       co2=nz*cos(angle)
                       si2=sin(angle)*sin(angle2)
                    end if
                    co2=co2/si
                    si2=si2/si
                 else 
                    co2=1
                    si2=0
                 end if
                 ax=((kappa*co**2+si**2)*co2**2+kappa*si2**2)/&
                      &SQRT(kappa**2*co**2*co2**2+kappa**2*si2**2+si**2*co2**2)*&
                      &(kappa*si2**2+co2**2*(kappa*co**2+si**2))
                 bx=-((kappa*co**2+si**2)*co2**2+kappa*si2**2)/&
                      &SQRT(kappa**2*co**2*co2**2+kappa**2*si2**2+si**2*co2**2)*&
                      &((kappa-1)*co2*si2*si**2)
                 cx=-((kappa*co**2+si**2)*co2**2+kappa*si2**2)/&
                      &SQRT(kappa**2*co**2*co2**2+kappa**2*si2**2+si**2*co2**2)*&
                      &((kappa-1)*co2*si*co)

                 ay=-((kappa-1)*co2*si2*si**2)/&
                      &SQRT(kappa**2*co**2*co2**2+kappa**2*si2**2+si**2*co2**2)*&
                      &(kappa*si2**2+co2**2*(kappa*co**2+si**2))
                 by=((kappa-1)*co2*si2*si**2)/&
                      &SQRT(kappa**2*co**2*co2**2+kappa**2*si2**2+si**2*co2**2)*&
                      &((kappa-1)*co2*si2*si**2)
                 cy=((kappa-1)*co2*si2*si**2)/&
                      &SQRT(kappa**2*co**2*co2**2+kappa**2*si2**2+si**2*co2**2)*&
                      &((kappa-1)*co2*si*co)


                 az=-((kappa-1)*co*si*co2)/&
                      &SQRT(kappa**2*co**2*co2**2+kappa**2*si2**2+si**2*co2**2)*&
                      &(kappa*si2**2+co2**2*(kappa*co**2+si**2))
                 bz=((kappa-1)*co*si*co2)/&
                      &SQRT(kappa**2*co**2*co2**2+kappa**2*si2**2+si**2*co2**2)*&
                      &((kappa-1)*co2*si2*si**2)
                 cz=((kappa-1)*co*si*co2)/&
                      &SQRT(kappa**2*co**2*co2**2+kappa**2*si2**2+si**2*co2**2)*&
                      &((kappa-1)*co2*si*co)


                    if(norm2d.gt.0) then
                       coeffi=ax*nx**2-bx*nx*ny/norm2d-cx*nx**2*nz/norm2d&
                            &-ay*nx*ny/norm2d+by*ny**2/norm2d**2+cy*ny*nx*nz/norm2d**2&
                            &-az*nx**2*nz/norm2d+ny*nx*nz*bz/norm2d**2+nx**2*nz**2&
                            &*cz/norm2d**2
                    else 
                       coeffi=cz
                    end if

                    !                    WRITE(6,*) '1',coeffi,nx
                    mat_aux(1:Nphi(degree_tri_loc(II))&
                         &,1:Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp&
                         &+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(2)**2/sqrt(sum(normal**2))*Vs1&
                         &+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(3)**2/sqrt(sum(normal**2))*Vs1)


                    !                    coeffi=d*nx**2+a*ny**2+(b+c)*nx*ny
                    !                    coeffi=bx*nx**2+ay*ny**2+bz*nz**2
                    if(norm2d.gt.0) then
                       coeffi=ax*nx*ny+bx*nx**2/norm2d-cx*nx*ny*nz/norm2d&
                            &-ay*ny**2/norm2d-by*ny*nx/norm2d**2+cy*ny**2*nz&
                            &/norm2d**2&
                            &-az*nx*ny*nz/norm2d-bz*nx**2*nz/norm2d**2+cz*nx*nz&
                            &**2*ny/norm2d**2
                    else 
                       coeffi=-bz*nz
                    end if

                    !                    WRITE(6,*) '2',coeffi


                    mat_aux(1:Nphi(degree_tri_loc(II))&
                         &,Nphi(degree_tri_loc(II))+1:2&
                         &*Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(1)*normal(2)/sqrt(sum(normal**2))&
                         &*Vs1)


                    !                    coeffi=cx*nx**2+cy*ny**2+az*nz**2
                    if(norm2d.gt.0) then
                       coeffi=ax*nx*nz+cx*nx*norm2d-ay*ny*nz/norm2d-cy*ny-az*nx&
                            &*nz**2/norm2d-cz*nx*nz
                    else 
                       coeffi=-az
                    end if
                    !                    WRITE(6,*) '3',coeffi
                    mat_aux(1:Nphi(degree_tri_loc(II))&
                         &,2*Nphi(degree_tri_loc(II))+1:3&
                         &*Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(1)*normal(3)/sqrt(sum(normal**2))&
                         &*Vs1)






                    !                    coeffi=c*nx**2-b*ny**2+(a-d)*nx*ny
                    !                    coeffi=ay*nx**2+bx*ny**2+cy*nz**2
                    if(norm2d.gt.0) then
                       coeffi=ax*nx*ny-bx*ny**2/norm2d-cx*nx*ny*nz/norm2d+&
                            &ay*nx**2/norm2d-by*ny*nx/norm2d**2-cy*nx**2*nz/norm2d**2&
                            &-az*nx*ny*nz/norm2d+bz*ny**2*nz/norm2d**2+cz*nx*nz**2*ny/norm2d**2
                    else 
                       coeffi=-cy*nz
                    end if

                    !                    WRITE(6,*) '4',coeffi
                    mat_aux(Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II))&
                         &,1:Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(1)*normal(2)/sqrt(sum(normal**2))&
                         &*Vs1)


                    !                    coeffi=b*nx**2-c*ny**2+(a-d)*nx*ny
                    !                    coeffi=by*nx**2+ax*ny**2+by*nz**2
                    if(norm2d.gt.0) then
                       coeffi=ax*ny**2+bx*nx*ny/norm2d-cx*ny**2*nz/norm2d+&
                            &ay*nx*ny/norm2d+by*nx**2/norm2d**2-cy*ny*nx*nz/norm2d**2&
                            &-az*ny**2*nz/norm2d-bz*ny*nx*nz/norm2d**2+cz*ny**2*nz&
                            &**2/norm2d**2
                    else 
                       coeffi=by
                    end if

                    !                    WRITE(6,*) '5',coeffi,ny
                    mat_aux(Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II))&
                         &,Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(1)**2/sqrt(sum(normal**2))*Vs1+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(3)**2/sqrt(sum(normal**2))*Vs1)


                    !                    coeffi=cy*nx**2+cx*ny**2+ay*nz**2
                    if(norm2d.gt.0) then
                       coeffi=ax*ny*nz+cx*ny*norm2d&
                            &+ay*nx*nz/norm2d+cy*nx&
                            &-az*ny*nz**2/norm2d-cz*ny*nz
                    else 
                       coeffi=ay*nz
                    end if
                    !                    WRITE(6,*) '6',coeffi

                    mat_aux(Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II))&
                         &,2*Nphi(degree_tri_loc(II))+1:3&
                         &*Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(2)*normal(3)/sqrt(sum(normal**2))&
                         &*Vs1)

                    !                    coeffi=b*nx**2-c*ny**2+(a-d)*nx*ny
                    !                    coeffi=az*nx**2+bz*ny**2+cx*nz**2
                    if(norm2d.gt.0) then
                       coeffi=ax*nx*nz-bx*ny*nz/norm2d-cx*nz**2*nx/norm2d+az*nx&
                            &*norm2d-bz*ny-cz*nx*nz
                    else 
                       coeffi=-cx
                    end if
                    !                    WRITE(6,*) '7',coeffi
                    mat_aux(2*Nphi(degree_tri_loc(II))+1:3*Nphi(degree_tri_loc(II))&
                         &,1:Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(1)*normal(3)/sqrt(sum(normal**2))&
                         &*Vs1)



                    !                    coeffi=bz*nx**2+az*ny**2+bx*nz**2
                    if(norm2d.gt.0) then
                       coeffi=ax*ny*nz+bx*nx*nz/norm2d-cx*nz**2*ny/norm2d&
                            &+az*ny*norm2d+bz*nx-cz*ny*nz
                    else 
                       coeffi=bx*nz
                    end if
                    !                    WRITE(6,*) '8',coeffi
                    mat_aux(2*Nphi(degree_tri_loc(II))+1:3*Nphi(degree_tri_loc(II))&
                         &,Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(2)*normal(3)/sqrt(sum(normal**2))&
                         &*Vs1)



                    !                    coeffi=b*nx**2-c*ny**2+(a-d)*nx*ny
                    !                    coeffi=cz*nx**2+cz*ny**2+ax*nz**2
                    if(norm2d.gt.0) then
                       coeffi=ax*nz**2+cx*nz*norm2d&
                            &+az*nz*norm2d+cz*(nx**2+ny**2)
                    else 
                       coeffi=ax
                    end if
                    !                    WRITE(6,*) '9',coeffi
                    mat_aux(2*Nphi(degree_tri_loc(II))+1:3*Nphi(degree_tri_loc(II))&
                         &,2*Nphi(degree_tri_loc(II))+1:3*Nphi(degree_tri_loc(II)))=rho(II)*&
                         &(matrices %matsurface(degree_tri_loc(II)) &
                         &%dkphiIdlphiJ_surf(1) &
                         &%coeff(1,1,:,: ,JJ)*coeffi*sqrt(sum(normal)**2)*Vp+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(1)**2/sqrt(sum(normal**2))*Vs1+&
                         &matrices &
                         &%matsurface(degree_tri_loc(II)) %dkphiIdlphiJ_surf(1)&
                         &%coeff(1,1 &
                         &,:,: ,JJ)*normal(2)**2/sqrt(sum(normal**2))*Vs1)




                    mat_aux(1:Nphi(degree_tri_loc(II))&
                         &,1:3*Nphi(degree_tri_loc(II)))=&
                         &MATMUL(Minv(degree_tri_loc(II))%coeff,mat_aux(1:Nphi(degree_tri_loc(II))&
                         &,1:3*Nphi(degree_tri_loc(II))))/DFVec/rho(II)


                    mat_aux(Nphi(degree_tri_loc(II))+1:2*Nphi(degree_tri_loc(II))&
                         &,1:3*Nphi(degree_tri_loc(II)))=&
                         &MATMUL(Minv(degree_tri_loc(II))%coeff&
                         &,mat_aux(Nphi(degree_tri_loc(II))+1:2&
                         &*Nphi(degree_tri_loc(II)),1:3&
                         &*Nphi(degree_tri_loc(II))))/DFVec/rho(II)

                    mat_aux(2*Nphi(degree_tri_loc(II))+1:3*Nphi(degree_tri_loc(II))&
                         &,1:3*Nphi(degree_tri_loc(II)))=&
                         &MATMUL(Minv(degree_tri_loc(II))%coeff&
                         &,mat_aux(2*Nphi(degree_tri_loc(II))+1:3&
                         &*Nphi(degree_tri_loc(II)),1:3&
                         &*Nphi(degree_tri_loc(II))))/DFVec/rho(II)

                    A_CLA_sol(tmp_sol-dim**2*Nphi(degree_tri_loc(II))**2+1:tmp_sol)&
                         &=A_CLA_sol(tmp_sol-dim**2*Nphi(degree_tri_loc(II))**2&
                         &+1:tmp_sol)+&
                         &RESHAPE( mat_aux(1:dim*Nphi(degree_tri_loc(II))&
                         &,1:dim*Nphi(degree_tri_loc(II))),&
                         &(/dim**2*Nphi(degree_tri_loc(II))**2/))

              end if !! End IF 2D/3D
           end if !! End IF acou/ela
        end if !! End IF the edge/face of the element carries an ABC

        if (TestCla==0) then
           NbnoCla=NbnoCla+1
        end if

     end do !! End of the loop on the edges/faces of the current element
  end do !! End of the loop on all the elements
end subroutine sub_CLA

