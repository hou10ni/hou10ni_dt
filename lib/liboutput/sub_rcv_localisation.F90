SUBROUTINE sub_rcv_localisation
  USE PRECISION
  USE mpi_modif
  USE DATA
  USE mesh, courbe=>curved
  USE solution
  USE POLYNOM
  USE BASIS_FUNCTIONS
  USE Matrix
  USE curved
  IMPLICIT NONE
  REAL(dq) :: res
  REAL(dp) :: rcv_coor(dim)
  REAL(dp),DIMENSION(:,:),ALLOCATABLE,SAVE :: coord_rcv_ref, coord_rcv_tmp,coord_rcv_ref_tmp
  LOGICAL, ALLOCATABLE ::TEST_TRI(:)
  INTEGER :: I,j,k,deg,nb_rcv_tmp,current_tri
  REAL(dp) ::xmax,ymax,zmax,xmin,ymin,zmin
  LOGICAL :: tag_continue,find_triangle

  ALLOCATE(I_rcv_GLOB(2,nb_rcv))
  ALLOCATE(I_rcv_LOC(nb_rcv))
  ALLOCATE(coord_rcv_ref_tmp(nb_rcv,dim))
  I_rcv_GLOB(2,:)=myrank
  I_rcv_GLOB(1,:)=0
  !We define a box containing the subdomain of the processor
  ALLOCATE(TEST_TRI(Ntri_loc))
  xmin=MINVAL(Coor(Tri_loc(:,1),1))
  xmax=MAXVAL(Coor(Tri_loc(:,1),1))
  ymin=MINVAL(Coor(Tri_loc(:,1),2))
  ymax=MAXVAL(Coor(Tri_loc(:,1),2))
  DO I=2,dim+1
     xmin=MIN(xmin,MINVAL(Coor(Tri_loc(:,I),1)))
     xmax=MAX(xmax,MAXVAL(Coor(Tri_loc(:,I),1)))
     ymin=MIN(ymin,MINVAL(Coor(Tri_loc(:,I),2)))
     ymax=MAX(ymax,MAXVAL(Coor(Tri_loc(:,I),2)))     
  END DO
  current_tri=1
  IF(dim.EQ.2) THEN
     DO j=1,nb_rcv
        rcv_coor=coord_rcv(j,:)
        !we check if the receiver is in a box containing the subdomain of the
        !processor
        IF((rcv_coor(1).GT.xmax).OR.(rcv_coor(1).LT.xmin)&
             &.OR.(rcv_coor(2).GT.ymax).OR.(rcv_coor(2).LT.ymin)) THEN
           CYCLE
        ENDIF
        TEST_TRI=.FALSE.
        tag_continue=.TRUE.

        DO WHILE (tag_continue)
           tag_continue=.FALSE.
           find_triangle=.TRUE.
           Test_tri(current_tri)=.TRUE.
           CALL sub_transform(Coor(Tri_loc(current_tri,:),:),rcv_coor&
                &,coord_rcv_ref_tmp(j&
                &,:))

           IF (coord_rcv_ref_tmp(j,1).LE.-0.0000001) THEN
              !the point is outside the triangle (above its second edge)
              find_triangle=.FALSE.
              IF ((Neigh_loc(current_tri,2).GT.0).AND.(Neigh_loc(current_tri,2).LE.Ntri_loc)) THEN
                 IF (.NOT.Test_tri(Neigh_loc(current_tri,2))) THEN
                    !we did not test the neighbourgh yet
                    current_tri=Neigh_loc(current_tri,2)
                    tag_continue=.TRUE.
                    CYCLE
                 END IF
              END IF
           END IF
           IF (coord_rcv_ref_tmp(j,2).LE.-0.0000001) THEN
              !the point is outside the triangle  (above its third edge)
              find_triangle=.FALSE.
              IF ((Neigh_loc(current_tri,3).GT.0).AND.(Neigh_loc(current_tri,3).LE.Ntri_loc)) THEN
                 IF (.NOT.Test_tri(Neigh_loc(current_tri,3))) THEN
                    !we did not test the neighbourgh yet
                    current_tri=Neigh_loc(current_tri,3)
                    tag_continue=.TRUE.
                    CYCLE
                 END IF
              END IF
           END IF
           IF (coord_rcv_ref_tmp(j,1)+coord_rcv_ref_tmp(j,2).GE.1.0000001) THEN
              !the point is outside the triangle (above its first edge)
              find_triangle=.FALSE.
              IF ((Neigh_loc(current_tri,1).GT.0).AND.(Neigh_loc(current_tri,1).LE.Ntri_loc)) THEN

                 IF (.NOT.Test_tri(Neigh_loc(current_tri,1))) THEN
                    !we did not test the neighbourgh yet
                    current_tri=Neigh_loc(current_tri,1)
                    tag_continue=.TRUE.
                    CYCLE
                 END IF
              END IF
           END IF
           IF (find_triangle) THEN
              I_rcv_Glob(1,j)=triloc2glob(current_tri)
              I_rcv_loc(j)=current_tri
           ELSE
              !We have to test a new triangle
              DO I=1,Ntri_loc
                 IF  (.NOT.Test_tri(I)) THEN
                    current_tri=I
                    tag_continue=.TRUE.
                 END IF
              END DO
           END IF
        ENDDO
     END DO
  ELSE
     !We define a box containing the subdomain of the processor
     zmin=MINVAL(Coor(Tri_loc(:,1),3))
     zmax=MAXVAL(Coor(Tri_loc(:,1),3))
     DO I=2,4
        zmin=MIN(zmin,MINVAL(Coor(Tri_loc(:,I),3)))
        zmax=MAX(zmax,MAXVAL(Coor(Tri_loc(:,I),3)))
     END DO
     current_tri=1
     DO j=1,nb_rcv
        rcv_coor=coord_rcv(j,:)
        !we check if the receiver is in a box containing the subdomain of the
        !processor
        IF((rcv_coor(1).GT.xmax).OR.(rcv_coor(1).LT.xmin)&
             &.OR.(rcv_coor(2).GT.ymax).OR.(rcv_coor(2).LT.ymin)&
             &.OR.(rcv_coor(3).GT.zmax).OR.(rcv_coor(3).LT.zmin)) THEN
           CYCLE
        ENDIF
        TEST_TRI=.FALSE.
        tag_continue=.TRUE.

        DO WHILE (tag_continue)
           tag_continue=.FALSE.
           find_triangle=.TRUE.
           Test_tri(current_tri)=.TRUE.
           CALL sub_transform(Coor(Tri_loc(current_tri,:),:),rcv_coor,coord_rcv_ref_tmp(j,:))
           IF (coord_rcv_ref_tmp(j,1).LE.-0.0000001) THEN
              !the point is outside the tetra (beyond its second face)
              find_triangle=.FALSE.
              IF (Neigh_loc(current_tri,2).GT.0) THEN
                 IF ((Neigh_loc(current_tri,2).GT.0).AND.(Neigh_loc(current_tri,2).LE.Ntri_loc)) THEN
                    !we did not test the neighbourgh yet
                    current_tri=Neigh_loc(current_tri,2)
                    tag_continue=.TRUE.
                    CYCLE
                 END IF
              END IF
           END IF
           IF (coord_rcv_ref_tmp(j,2).LE.-0.0000001) THEN
              !the point is outside the triangle  (beyond its third face)
              find_triangle=.FALSE.
              IF ((Neigh_loc(current_tri,3).GT.0).AND.(Neigh_loc(current_tri,3).LE.Ntri_loc)) THEN
                 IF (.NOT.Test_tri(Neigh_loc(current_tri,3))) THEN
                    !we did not test the neighbourgh yet
                    current_tri=Neigh_loc(current_tri,3)
                    tag_continue=.TRUE.
                    CYCLE
                 END IF
              END IF
           END IF
           IF (coord_rcv_ref_tmp(j,3).LE.-0.0000001) THEN
              !the point is outside the triangle  (beyond its fourth face)
              find_triangle=.FALSE.
              IF ((Neigh_loc(current_tri,4).GT.0).AND.(Neigh_loc(current_tri,4).LE.Ntri_loc)) THEN
                 IF (.NOT.Test_tri(Neigh_loc(current_tri,4))) THEN
                    !we did not test the neighbourgh yet
                    current_tri=Neigh_loc(current_tri,4)
                    tag_continue=.TRUE.
                    CYCLE
                 END IF
              END IF
           END IF
           IF (coord_rcv_ref_tmp(j,1)+coord_rcv_ref_tmp(j,2)&
                &+coord_rcv_ref_tmp(j,3).GE.1.0000001) THEN
              !the point is outside the triangle (above its first face)
              find_triangle=.FALSE.
              IF ((Neigh_loc(current_tri,1).GT.0).AND.(Neigh_loc(current_tri,1).LE.Ntri_loc)) THEN
                 IF (.NOT.Test_tri(Neigh_loc(current_tri,1))) THEN
                    !we did not test the neighbourgh yet
                    current_tri=Neigh_loc(current_tri,1)
                    tag_continue=.TRUE.
                    CYCLE
                 END IF
              END IF
           END IF
           IF (find_triangle) THEN
              I_rcv_Glob(2,j)=myrank
              I_rcv_Glob(1,j)=triloc2glob(current_tri)
              I_rcv_Glob(2,j)=myrank
              I_rcv_loc(j)=current_tri
           ELSE
              !We have to test a new triangle
              DO I=1,Ntri_loc
                 IF  (.NOT.Test_tri(I)) THEN
                    current_tri=I
                    tag_continue=.TRUE.
                 END IF
              END DO
           END IF
        ENDDO
     END DO
  END IF
  CALL MPI_ALLREDUCE(MPI_IN_PLACE,I_rcv_glob , nb_rcv, MPI_2INTEGER,&
       & MPI_MAXLOC, MPI_COMM_WORLD, ierr ); 
  nb_rcv_tmp=0
  DO I=1,nb_rcv
     IF((I_rcv_glob(2,I).EQ.myrank).AND.(I_rcv_glob(1,I).NE.0))  THEN
        nb_rcv_tmp=nb_rcv_tmp+1
     END IF
  END DO
  ALLOCATE(I_rcv(nb_rcv_tmp))
  ALLOCATE(Idx_rcv_glob(nb_rcv_tmp))
  ALLOCATE(coord_rcv_ref(nb_rcv_tmp,dim))
  ALLOCATE(coord_rcv_tmp(nb_rcv_tmp,dim))
  nb_rcv_tmp=0
  DO I=1,nb_rcv
     IF((I_rcv_glob(2,I).EQ.myrank).AND.(I_rcv_glob(1,I).NE.0)) THEN
        nb_rcv_tmp=nb_rcv_tmp+1
        I_rcv(nb_rcv_tmp)=I_rcv_loc(I)
        coord_rcv_ref(nb_rcv_tmp,:)=coord_rcv_ref_tmp(I,:)
        coord_rcv_tmp(nb_rcv_tmp,:)=coord_rcv(I,:)
        Idx_rcv_glob(nb_rcv_tmp)=I
     END IF
  END DO
  nb_rcv=nb_rcv_tmp

  DEALLOCATE(coord_rcv)
  ALLOCATE(coord_rcv(nb_rcv,dim))
  coord_rcv=coord_rcv_tmp
  nb_rcvflu=0
  nb_rcvsol=0
  ALLOCATE(val_phi_rcv(nb_rcv))
  
  DO I=1,nb_rcv
     deg=degree_tri_loc(I_rcv(I))
     IF(acouela(ref_media_loc(I_rcv(I))).EQ.1) THEN
        nb_rcvflu=nb_rcvflu+1
     ELSE
        nb_rcvsol=nb_rcvsol+1
     END IF
     ALLOCATE(val_phi_rcv(I)%val(Nphi(deg)))
     IF(dim == 2) THEN
        DO k=1,Nphi(deg)
           val_phi_rcv(I)%val(k) = valpoly_p(basis(deg)%p(k),real(coord_rcv_ref(I,1:2),dq))
        END DO
     ELSE 
        DO k=1,Nphi(deg)
           val_phi_rcv(I)%val(k) = valpoly_p(basis(deg)%p(k),real(coord_rcv_ref(I,1:3),dq))
        END DO
     END IF
  ENDDO
END SUBROUTINE sub_rcv_localisation
