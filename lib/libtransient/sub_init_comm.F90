SUBROUTINE sub_init_comm
  
  USE PRECISION
    USE CONSTANT
  USE DATA
    USE mesh
  USE mpi_modif
  USE transient
  IMPLICIT NONE


  

  INTEGER :: Procineigh_loc,Ineigh, I,J,i_tmp3,i_tmp4


  ALLOCATE(comm_ghost_flu(Nprocneigh_flu))
  ALLOCATE(comm_ghost_rec_flu(Nprocneigh_flu))
  ALLOCATE(comm_ghost_send2rcv_flu(Nprocneigh_flu))
  ALLOCATE(comm_ghost_sol(Nprocneigh_sol))
  ALLOCATE(comm_ghost_rec_sol(Nprocneigh_sol))
  ALLOCATE(comm_ghost_send2rcv_sol(Nprocneigh_sol))
  ALLOCATE(idx_ghost_flu(Nprocneigh_flu))
  ALLOCATE(idx_ghost_sol(Nprocneigh_sol))
  ALLOCATE(tag_send_sol(Nprocneigh_sol))
  ALLOCATE(tag_recv_sol(Nprocneigh_sol))
  ALLOCATE(req_send_sol(Nprocneigh_sol))
  ALLOCATE(req_recv_sol(Nprocneigh_sol))
  ALLOCATE(tag_send_flu(Nprocneigh_flu))
  ALLOCATE(tag_recv_flu(Nprocneigh_flu))
  ALLOCATE(req_send_flu(Nprocneigh_flu))
  ALLOCATE(req_recv_flu(Nprocneigh_flu))
  DO I=1,Nprocneigh_sol
           tag_send_sol(I)=myrank*nproc+neighproc_sol(I)-1
           tag_recv_sol(I)=(neighproc_sol(I)-1)*nproc+myrank
     END DO
     DO I=1,Nprocneigh_flu
           tag_send_flu(I)=myrank*nproc+neighproc_flu(I)-1
           tag_recv_flu(I)=(neighproc_flu(I)-1)*nproc+myrank
     END DO

     Nsol_total=Nsolflu_inner+Nsol_inner+Nsolflu_border+Nsol_border
     Nflu_total=Nflusol_inner+Nflu_inner+Nflusol_border+Nflu_border
     idx_ghost_flu=0
     idx_ghost_sol=0
!!!Loop on all the fluid border elements
     DO I=Nflusol_inner+Nflu_inner+1,Nflu_total
        DO J=1,Nfacesperelem
           Ineigh=Neigh_loc(I,J)
           IF(Ineigh.GT.Nflu_total+Nsol_total) THEN
!!! If the neigh is a ghost
              IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nflu_ghost)) THEN
!!! If the neigh is in the fluid
                 DO Procineigh_loc=1,Nprocneigh_flu
                    IF (ineigh.LE.Nflu_total+Nsol_total&
                         &+SUM(Nflu_ghost(1:Procineigh_loc))) THEN
                       !! I is the ghost of Ineigh for processor Procineigh_loc
                       idx_ghost_flu(Procineigh_loc)&
                            &=idx_ghost_flu(Procineigh_loc)&
                            &+Nphi(Degree_Tri_loc(I))
                       EXIT
                    END IF
                 END DO
              ELSE
!!! If the neigh is in the solid
                 DO Procineigh_loc=1,Nprocneigh_sol
                    IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nsol_ghost(1:Procineigh_loc))) THEN
                       !! I is the ghost of Ineigh for processor Procineigh_loc
                       idx_ghost_sol(Procineigh_loc)&
                            &=idx_ghost_sol(Procineigh_loc)+dim&
                            &*Nphi(Degree_Tri_loc(I))
                       EXIT
                    END IF
                 END DO
              END IF
           END IF
        END DO
     END DO

!!!Loop on all the solid border elements
     DO I=Nflu_total+Nsolflu_inner+Nsol_inner+1,Nflu_total+Nsol_total
        DO J=1,Nfacesperelem
           Ineigh=Neigh_loc(I,J)
           IF(Ineigh.GT.Nflu_total+Nsol_total) THEN
!!! If the neigh is a ghost
              IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nflu_ghost)) THEN
!!! If the neigh is in the fluid
                 DO Procineigh_loc=1,Nprocneigh_flu
                    IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nflu_ghost(1:Procineigh_loc))) THEN
                       !! I is the ghost of Ineigh for processor Procineigh_loc
                       idx_ghost_flu(Procineigh_loc)&
                            &=idx_ghost_flu(Procineigh_loc)&
                            &+Nphi(Degree_Tri_loc(I))
                       EXIT
                    END IF
                 END DO
              ELSE
!!! If the neigh is in the solid
                 DO Procineigh_loc=1,Nprocneigh_sol
                    IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nsol_ghost(1:Procineigh_loc))) THEN
                       !! I is the ghost of Ineigh for processor Procineigh_loc
                       idx_ghost_sol(Procineigh_loc)&
                            &=idx_ghost_sol(Procineigh_loc)+dim&
                            &*Nphi(Degree_Tri_loc(I))
                       EXIT
                    END IF
                 END DO
              END IF
           END IF
        END DO
     END DO
     i_tmp3=index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border&
          &+Nsol_border)+1
     DO Procineigh_loc=1,Nprocneigh_sol
        ALLOCATE(comm_ghost_sol(Procineigh_loc)&
             &%VALUE(idx_ghost_sol(Procineigh_loc)*nb_rhs))
        ALLOCATE(comm_ghost_send2rcv_sol(Procineigh_loc)%VALUE(2&
             &,Nsol_ghost(Procineigh_loc)))
        i_tmp4=index_tri_loc_sol(Nsolflu_inner+Nsol_inner+Nsolflu_border&
              &+Nsol_border+SUM(Nsol_ghost(1:Procineigh_loc)))
        ALLOCATE(comm_ghost_rec_sol(Procineigh_loc)&
             &%VALUE((i_tmp4-i_tmp3+1)*nb_rhs))
        comm_ghost_rec_sol(Procineigh_loc)%Value=0
        comm_ghost_sol(Procineigh_loc)%VALUE=0
         i_tmp3=i_tmp4+1    
     END DO
     i_tmp3=index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border&
          &+Nflusol_border)+1
     DO Procineigh_loc=1,Nprocneigh_flu
        ALLOCATE(comm_ghost_flu(Procineigh_loc)&
             &%VALUE(idx_ghost_flu(Procineigh_loc)*nb_rhs))
        ALLOCATE(comm_ghost_send2rcv_flu(Procineigh_loc)%VALUE(2,Nflu_ghost(Procineigh_loc)))
        i_tmp4=index_tri_loc_flu(Nflu_inner+Nflusol_inner+Nflu_border&
             &+Nflusol_border+SUM(Nflu_ghost(1:Procineigh_loc)))
        ALLOCATE(comm_ghost_rec_flu(Procineigh_loc)&
             &%VALUE((i_tmp4-i_tmp3+1)*nb_rhs))
        comm_ghost_rec_flu(Procineigh_loc)%Value=0
        comm_ghost_flu(Procineigh_loc)%Value=0
        i_tmp3=i_tmp4+1
     END DO


     idx_ghost_flu=0
     idx_ghost_sol=0
!!!Loop on all the fluid border elements
     DO I=Nflusol_inner+Nflu_inner+1,Nflu_total
        DO J=1,Nfacesperelem
           Ineigh=Neigh_loc(I,J)
           IF(Ineigh.GT.Nflu_total+Nsol_total) THEN
!!! If the neigh is a ghost
              IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nflu_ghost)) THEN
!!! If the neigh is in the fluid
                 DO Procineigh_loc=1,Nprocneigh_flu
                    IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nflu_ghost(1:Procineigh_loc))) THEN
                       !! I is the ghost of Ineigh for processor Procineigh_loc
                       idx_ghost_flu(Procineigh_loc)=idx_ghost_flu(Procineigh_loc)+1
                       comm_ghost_send2rcv_flu(Procineigh_loc)%VALUE(1,idx_ghost_flu(Procineigh_loc))&
                            &=idx_vec_flu(2,I)
                       comm_ghost_send2rcv_flu(Procineigh_loc)%VALUE(2,idx_ghost_flu(Procineigh_loc))&
                            &=idx_vec_flu(3,I)
                       EXIT
                    END IF
                 END DO
              ELSE 
!!! If the neigh is in the solid
                 DO Procineigh_loc=1,Nprocneigh_sol
                    IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nsol_ghost(1:Procineigh_loc))) THEN
                       !! I is the ghost of Ineigh for processor Procineigh_loc
                       idx_ghost_sol(Procineigh_loc)=idx_ghost_sol(Procineigh_loc)+1
                       comm_ghost_send2rcv_sol(Procineigh_loc)%VALUE(1,idx_ghost_sol(Procineigh_loc))&
                            &=idx_vec_sol(2,I)
                       comm_ghost_send2rcv_sol(Procineigh_loc)%VALUE(2,idx_ghost_sol(Procineigh_loc))&
                            &=idx_vec_sol(3,I)
                       EXIT
                    END IF
                 END DO
              END IF
           END IF
        END DO
     END DO


!!!Loop on all the solid border elements
     DO I=Nflu_total+Nsolflu_inner+Nsol_inner+1,Nflu_total+Nsol_total
        DO J=1,Nfacesperelem
           Ineigh=Neigh_loc(I,J)
           IF(Ineigh.GT.Nflu_total+Nsol_total) THEN
!!! If the neigh is a ghost
              IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nflu_ghost)) THEN
                 WRITE(6,*) ineigh,Nflu_total+Nsol_total,Nflu_total+Nsol_total+SUM(Nflu_ghost)
!!! If the neigh is in the fluid
                 DO Procineigh_loc=1,Nprocneigh_flu
                    IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nflu_ghost(1:Procineigh_loc))) THEN
                       !! I is the ghost of Ineigh for processor Procineigh_loc
                       idx_ghost_flu(Procineigh_loc)=idx_ghost_flu(Procineigh_loc)+1
                       comm_ghost_send2rcv_flu(Procineigh_loc)%VALUE(1,idx_ghost_flu(Procineigh_loc))&
                            &=idx_vec_flu(2,I)
                       comm_ghost_send2rcv_flu(Procineigh_loc)%VALUE(2,idx_ghost_flu(Procineigh_loc))&
                            &=idx_vec_flu(3,I)
                       EXIT
                    END IF
                 END DO
              ELSE 
!!! If the neigh is in the solid
                 DO Procineigh_loc=1,Nprocneigh_sol
                    IF (ineigh.LE.Nflu_total+Nsol_total+SUM(Nsol_ghost(1:Procineigh_loc))) THEN
                       !! I is the ghost of Ineigh for processor Procineigh_loc
                       idx_ghost_sol(Procineigh_loc)=idx_ghost_sol(Procineigh_loc)+1
                       comm_ghost_send2rcv_sol(Procineigh_loc)%VALUE(1,idx_ghost_sol(Procineigh_loc))&
                            &=idx_vec_sol(2,I)
                       comm_ghost_send2rcv_sol(Procineigh_loc)%VALUE(2,idx_ghost_sol(Procineigh_loc))&
                            &=idx_vec_sol(3,I)
                       EXIT
                    END IF
                 END DO
              END IF
           END IF
        END DO
     END DO  
   END SUBROUTINE sub_init_comm
