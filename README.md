# Hou10ni_dt
Hou10ni\_dt is a wave propagation simulation software using a Discontinuous Galerkin method. It is written in Fortran 90.

## Dependencies
- Metis or Scotchmetis 6.1.1 (define `-DWITH\_SCOTCHMETIS:BOOL=TRUE`)
- BLAS & Lapack (tested against Openblas)

## Install
```bash
cmake -B build -DMETIS_LIB_DIR=/path/to/metis/lib/dir -DMETIS_INC_DIR=/path/to/metis/header/dir && make -C build -j9
```
## Test
```bash
cd build && ctest
```

## Authors
- Julien Diaz for main parts
- Marc Fuentes for CMake

# run a simulation
```bash
cd build && mpirun -np 2 ./hou10ni.out input.txt
```
## License
The licence is CeCILL-C : you could found the licence text  in the file `Licence-en.txt`, in French in the file `Licence-fr.txt`
